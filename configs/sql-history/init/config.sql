-- CREATE TABLE "config" ---------------------------------------
CREATE TABLE `config`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`type` Enum( 'user_file', 'user_verification', 'user_role', 'user_terms', 'company_business_type', 'company_employee_size', 'carbon_emission_heating', 'carbon_emission_faciltity', 'carbon_emission_vehicle' ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`name` VarChar( 64 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`description` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`parent_id` Int( 0 ) NULL DEFAULT NULL,
	`createdAt` DateTime NOT NULL,
	`updatedAt` DateTime NOT NULL,
	`deletedAt` DateTime,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------;

-- CREATE INDEX "idx_type" --------------------------------
CREATE INDEX `idx_type` USING BTREE ON `config`( `type` );-- -------------------------------------------------------------

-- CREATE INDEX "idx_parent_id" --------------------------------
CREATE INDEX `idx_parent_id` USING BTREE ON `config`( `parent_id` );-- -------------------------------------------------------------

-- CREATE TABLE "domain_event" -----------------------------------------
CREATE TABLE `domain_event`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`pk` Int( 0 ) NOT NULL,
	`aggregate` Enum( 'user', 'company', 'carbon_emission' ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`name` VarChar( 32 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`status` Enum( 'ready', 'progress', 'done', 'fail' ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`errorLog` VarChar( 10000 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
	`processedAt` DateTime,
	`createdAt` DateTime NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE INDEX "idx_status" ------------------------------
CREATE INDEX `idx_status` ON `domain_event`( `status` );-- -------------------------------------------------------------

