-- CREATE TABLE "user" -----------------------------------------
CREATE TABLE `user`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`email` VarChar( 32 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`password` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`name` VarChar( 32 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`jobTitle` VarChar( 32 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`phone` VarChar( 32 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`company_id` Int( 0 ) NOT NULL,
	`createdAt` DateTime NOT NULL,
	`updatedAt` DateTime NOT NULL,
	`deletedAt` DateTime,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `ux_email` UNIQUE( `email` ),
	CONSTRAINT `ux_phone` UNIQUE( `phone` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE INDEX "idx_company_id" ------------------------------
CREATE INDEX `idx_company_id` ON `user`( `company_id` );-- -------------------------------------------------------------

-- CREATE TABLE "user_file" -----------------------------------------
CREATE TABLE `user_file`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`storagePath` VarChar( 64 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`user_id` Int( 0 ) NOT NULL,
	`config_id` Int( 0 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE LINK "fk_config_user_file" --------------------------
ALTER TABLE `user_file`
	ADD CONSTRAINT `fk_config_user_file` FOREIGN KEY ( `config_id` )
	REFERENCES `config`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE LINK "fk_user_user_file" ----------------------------
ALTER TABLE `user_file`
	ADD CONSTRAINT `fk_user_user_file` FOREIGN KEY ( `user_id` )
	REFERENCES `user`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE TABLE "user_verification" -----------------------------------------
CREATE TABLE `user_verification`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`user_id` Int( 0 ) NOT NULL,
	`config_id` Int( 0 ) NOT NULL,
	`value` VarChar( 32 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`verificationCode` VarChar( 8 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`isVerified` TinyInt( 1 ) NOT NULL,
	`sentAt` DateTime,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE LINK "fk_config_user_verification" --------------------------
ALTER TABLE `user_verification`
	ADD CONSTRAINT `fk_config_user_verification` FOREIGN KEY ( `config_id` )
	REFERENCES `config`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE LINK "fk_user_user_verification" ----------------------------
ALTER TABLE `user_verification`
	ADD CONSTRAINT `fk_user_user_verification` FOREIGN KEY ( `user_id` )
	REFERENCES `user`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE TABLE "user_role" -----------------------------------------
CREATE TABLE `user_role`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`user_id` Int( 0 ) NOT NULL,
	`config_id` Int( 0 ) NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `ux_user_id_config_id` UNIQUE( `user_id`, `config_id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE LINK "fk_config_user_role" --------------------------
ALTER TABLE `user_role`
	ADD CONSTRAINT `fk_config_user_role` FOREIGN KEY ( `config_id` )
	REFERENCES `config`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE LINK "fk_user_user_role" ----------------------------
ALTER TABLE `user_role`
	ADD CONSTRAINT `fk_user_user_role` FOREIGN KEY ( `user_id` )
	REFERENCES `user`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE TABLE "user_terms" -----------------------------------------
CREATE TABLE `user_terms`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`user_id` Int( 0 ) NOT NULL,
	`config_id` Int( 0 ) NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `ux_user_id_config_id` UNIQUE( `user_id`, `config_id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE LINK "fk_config_user_terms" --------------------------
ALTER TABLE `user_terms`
	ADD CONSTRAINT `fk_config_user_terms` FOREIGN KEY ( `config_id` )
	REFERENCES `config`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE LINK "fk_user_user_terms" ----------------------------
ALTER TABLE `user_terms`
	ADD CONSTRAINT `fk_user_user_terms` FOREIGN KEY ( `user_id` )
	REFERENCES `user`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------
