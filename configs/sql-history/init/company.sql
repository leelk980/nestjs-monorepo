-- CREATE TABLE "company" -----------------------------------------
CREATE TABLE `company`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 32 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`address` VarChar( 128 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`phone` VarChar( 32 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`annualSales` Int( 0 ) NOT NULL,
	`createdAt` DateTime NOT NULL,
	`updatedAt` DateTime NOT NULL,
	`deletedAt` DateTime,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE TABLE "company_business_type" -----------------------------------------
CREATE TABLE `company_business_type`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`company_id` Int( 0 ) NOT NULL,
	`config_id` Int( 0 ) NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `ux_company_id_config_id` UNIQUE( `company_id`, `config_id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE LINK "fk_config_company_business_type" --------------------------
ALTER TABLE `company_business_type`
	ADD CONSTRAINT `fk_config_company_business_type` FOREIGN KEY ( `config_id` )
	REFERENCES `config`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE LINK "fk_company_company_business_type" ----------------------------
ALTER TABLE `company_business_type`
	ADD CONSTRAINT `fk_company_company_business_type` FOREIGN KEY ( `company_id` )
	REFERENCES `company`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE TABLE "company_employee_size" -----------------------------------------
CREATE TABLE `company_employee_size`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`company_id` Int( 0 ) NOT NULL,
	`config_id` Int( 0 ) NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `ux_company_id_config_id` UNIQUE( `company_id`, `config_id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE LINK "fk_config_company_employee_size" --------------------------
ALTER TABLE `company_employee_size`
	ADD CONSTRAINT `fk_config_company_employee_size` FOREIGN KEY ( `config_id` )
	REFERENCES `config`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE LINK "fk_company_company_employee_size" ----------------------------
ALTER TABLE `company_employee_size`
	ADD CONSTRAINT `fk_company_company_employee_size` FOREIGN KEY ( `company_id` )
	REFERENCES `company`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------
