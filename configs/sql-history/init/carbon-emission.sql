-- CREATE TABLE "carbon_emission" -----------------------------------------
CREATE TABLE `carbon_emission`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`startDate` Date NOT NULL,
	`endDate` Date NOT NULL,
	`electricityUsage` Int( 0 ) NOT NULL,
	`electricityFee` Int( 0 ) NOT NULL,
	`renewableEnergyPercent` Int( 0 ) NOT NULL,
	`caluatedValue` Decimal( 12, 3 ) NOT NULL,
	`user_id` Int( 0 ) NOT NULL,
  `createdAt` DateTime NOT NULL,
	`updatedAt` DateTime NOT NULL,
	`deletedAt` DateTime,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE INDEX "idx_user_id" ------------------------------
CREATE INDEX `idx_user_id` ON `carbon_emission`( `user_id` );-- -------------------------------------------------------------

-- CREATE TABLE "carbon_emission_heating" -----------------------------------------
CREATE TABLE `carbon_emission_heating`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`usage` Int( 0 ) NOT NULL,
	`carbon_emission_id` Int( 0 ) NOT NULL,
	`config_id` Int( 0 ) NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `ux_carbon_emission_id_config_id` UNIQUE( `carbon_emission_id`, `config_id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE LINK "fk_config_carbon_emission_heating" --------------------------
ALTER TABLE `carbon_emission_heating`
	ADD CONSTRAINT `fk_config_carbon_emission_heating` FOREIGN KEY ( `config_id` )
	REFERENCES `config`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE LINK "fk_carbon_emission_carbon_emission_heating" ----------------------------
ALTER TABLE `carbon_emission_heating`
	ADD CONSTRAINT `fk_carbon_emission_carbon_emission_heating` FOREIGN KEY ( `carbon_emission_id` )
	REFERENCES `carbon_emission`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE TABLE "carbon_emission_facility" -----------------------------------------
CREATE TABLE `carbon_emission_facility`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`count` TinyInt( 0 ) NOT NULL,
	`usage` Int( 0 ) NOT NULL,
	`carbon_emission_id` Int( 0 ) NOT NULL,
	`config_id` Int( 0 ) NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `ux_carbon_emission_id_config_id` UNIQUE( `carbon_emission_id`, `config_id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE LINK "fk_config_carbon_emission_facility" --------------------------
ALTER TABLE `carbon_emission_facility`
	ADD CONSTRAINT `fk_config_carbon_emission_facility` FOREIGN KEY ( `config_id` )
	REFERENCES `config`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE LINK "fk_carbon_emission_carbon_emission_facility" ----------------------------
ALTER TABLE `carbon_emission_facility`
	ADD CONSTRAINT `fk_carbon_emission_carbon_emission_facility` FOREIGN KEY ( `carbon_emission_id` )
	REFERENCES `carbon_emission`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE TABLE "carbon_emission_vehicle" -----------------------------------------
CREATE TABLE `carbon_emission_vehicle`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`count` TinyInt( 0 ) NOT NULL,
	`usage` Int( 0 ) NOT NULL,
	`carbon_emission_id` Int( 0 ) NOT NULL,
	`config_id` Int( 0 ) NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `ux_carbon_emission_id_config_id` UNIQUE( `carbon_emission_id`, `config_id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE LINK "fk_config_carbon_emission_vehicle" --------------------------
ALTER TABLE `carbon_emission_vehicle`
	ADD CONSTRAINT `fk_config_carbon_emission_vehicle` FOREIGN KEY ( `config_id` )
	REFERENCES `config`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE LINK "fk_carbon_emission_carbon_emission_vehicle" ----------------------------
ALTER TABLE `carbon_emission_vehicle`
	ADD CONSTRAINT `fk_carbon_emission_carbon_emission_vehicle` FOREIGN KEY ( `carbon_emission_id` )
	REFERENCES `carbon_emission`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------
