SELECT concat('DROP TABLE IF EXISTS `', table_name, '`;')
FROM information_schema.tables
WHERE table_schema = 'dev_db';

SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS `carbon_emission`;
DROP TABLE IF EXISTS `carbon_emission_facility`;
DROP TABLE IF EXISTS `carbon_emission_heating`;
DROP TABLE IF EXISTS `carbon_emission_vehicle`;
DROP TABLE IF EXISTS `company`;
DROP TABLE IF EXISTS `company_business_type`;
DROP TABLE IF EXISTS `company_employee_size`;
DROP TABLE IF EXISTS `config`;
DROP TABLE IF EXISTS `domain_event`;
DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `user_file`;
DROP TABLE IF EXISTS `user_verification`;
DROP TABLE IF EXISTS `user_role`;
DROP TABLE IF EXISTS `user_terms`;
SET FOREIGN_KEY_CHECKS = 1;
