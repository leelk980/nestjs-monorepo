import { execSync } from 'child_process';
import * as fs from 'fs/promises';

const bootstrap = async () => {
  const param = process.argv[2]?.split('--')[1];
  if (!param || !['admin', 'backend'].includes(param)) {
    throw Error(`invalid process argv[2]: ${param}.`);
  }
  console.log(`Build ${param} sdk...`);

  // 1. sdk & swagger
  const packagePath = `packages/${param}-sdk`;

  await fs.rm(`${packagePath}/api`, { recursive: true }).catch((err) => {
    if (err.code !== 'ENOENT') throw err;
  });
  await fs.mkdir(`${packagePath}/api`, { recursive: true }).catch((err) => {
    if (err.code !== 'ENOENT') throw err;
  });

  execSync(`nestia sdk apps/${param}/src/**/*.controller.ts --out ${packagePath}/api`);
  execSync(`nestia swagger apps/${param}/src/**/*.controller.ts --out ${packagePath}`);

  // 2. build
  await fs.rm(`${packagePath}/dist`, { recursive: true }).catch((err) => {
    if (err.code !== 'ENOENT') throw err;
  });
  await fs.mkdir(`${packagePath}/dist`, { recursive: true }).catch((err) => {
    if (err.code !== 'ENOENT') throw err;
  });

  execSync(`tsc -p ./${packagePath}/tsconfig.package.json`);
};

bootstrap();
