import { ArrayUtil } from '@lib/common';
import { PrismaClient } from '@prisma/client';
import { execSync } from 'child_process';
import { log } from 'console';
import { rm, writeFile } from 'fs/promises';

process.env['DATABASE_URL'] = 'mysql://root:root1234@localhost:3306/dev_db';

function snakeToPascal(snakeCaseString: string): string {
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const camelCaseString = snakeCaseString.replace(/(\_\w)/g, (match) => match[1]!.toUpperCase());
  return camelCaseString.charAt(0).toUpperCase() + camelCaseString.slice(1);
}

async function bootstrap() {
  log('Build config enums...');
  const prismaClient = new PrismaClient({});

  const configMap = ArrayUtil.groupBy((each) => each.type, await prismaClient.config.findMany({}));
  const configTypes = Object.keys(configMap) as (keyof typeof configMap)[];

  const filePath = 'libs/common/src/global/config.enum.ts';
  const text = configTypes.reduce((acc, type) => {
    const names = configMap[type].map((each) => `'${each.name}'`);
    return acc + `\nexport type ${snakeToPascal(type)}Enum = ${names.join(' | ')};\n`;
  }, '');

  await rm(filePath, {}).catch((err) => {
    if (err.code !== 'ENOENT') throw err;
  });
  await writeFile(filePath, text).catch((err) => {
    if (err.code !== 'ENOENT') throw err;
  });
  execSync(`prettier --write ${filePath}`);
}

bootstrap();
