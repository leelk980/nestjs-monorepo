import { environment } from '@lib/common';
import { execSync } from 'child_process';

const { username, password, host, port, name } = environment.database;

process.env['DATABASE_URL'] = `mysql://${username}:${password}@${host}:${port}/${name}`;

console.log('Build prisma client...');

const schemaPath = 'libs/infrastructure/src/repository/impl/schema.prisma';

execSync(`prisma db pull --schema=${schemaPath}`);
execSync(`prisma generate --schema=${schemaPath}`);
