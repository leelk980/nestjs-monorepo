import { AdminService } from '@admin/admin.service';
import { TypedBody, TypedRoute } from '@nestia/core';
import { Controller } from '@nestjs/common';

@Controller()
export class AdminController {
  constructor(private readonly adminService: AdminService) {}

  @TypedRoute.Post()
  postHello(@TypedBody() data: PostHelloBody): string {
    console.log(data);

    return this.adminService.getHello();
  }
}

export interface PostHelloBody {
  name: string;
  gender: number;
}
