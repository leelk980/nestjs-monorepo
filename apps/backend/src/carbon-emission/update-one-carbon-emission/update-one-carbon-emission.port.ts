import { AsyncStorageManager, CarbonEmissionRepository, PrimaryPort } from '@lib/application';
import { Transaction } from '@lib/infrastructure';
import { ForbiddenException, Injectable } from '@nestjs/common';
import {
  UpdateOneCarbonEmissionData,
  UpdateOneCarbonEmissionView,
} from './update-one-carbon-emission.type';

@Injectable()
export class UpdateOneCarbonEmissionPort implements PrimaryPort {
  constructor(
    private readonly asyncStorageManager: AsyncStorageManager,
    private readonly carbonEmissionRepository: CarbonEmissionRepository,
  ) {}

  @Transaction()
  async execute(data: UpdateOneCarbonEmissionData): Promise<UpdateOneCarbonEmissionView> {
    const { id, startDate, endDate, ...etc } = data;

    const carbonEmissionAgg = await this.carbonEmissionRepository.findOneById(id);

    this.checkOwnerPermission(carbonEmissionAgg.userId);

    carbonEmissionAgg.update({
      startDate: startDate ? new Date(startDate) : carbonEmissionAgg.startDate,
      endDate: endDate ? new Date(endDate) : carbonEmissionAgg.endDate,
      ...etc,
    });

    await this.carbonEmissionRepository.saveOne(carbonEmissionAgg);

    return { id: carbonEmissionAgg.id };
  }

  private checkOwnerPermission(ownerId: number) {
    const currUser = this.asyncStorageManager.getItem('currUser');
    if (currUser.id !== ownerId) {
      throw new ForbiddenException(`no permission for updating carbon emission. ${currUser.id}`);
    }

    return true;
  }
}
