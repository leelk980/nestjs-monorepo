import {
  CarbonEmissionFaciltityEnum,
  CarbonEmissionHeatingEnum,
  CarbonEmissionVehicleEnum,
} from '@lib/common';

export interface UpdateOneCarbonEmissionData {
  /** @minimum 0 */
  id: number;

  /** @format date */
  startDate?: string;

  /** @format date */
  endDate?: string;

  /** @minimum 0 */
  electricityUsage?: number;

  /** @minimum 0 */
  electricityFee?: number;

  /** @minimum 0 */
  renewableEnergyPercent?: number;

  /** @minimum 0 */
  caluatedValue?: number;

  /** @minItems 1 */
  /** @maxItems 1 */
  carbonEmissionHeatings?: {
    type: CarbonEmissionHeatingEnum;

    /** @minimum 0 */
    usage: number;
  }[];

  /** @minItems 3 */
  /** @maxItems 3 */
  carbonEmissionFacilities?: {
    type: CarbonEmissionFaciltityEnum;

    /** @minimum 0 */
    count: number;

    /** @minimum 0 */
    usage: number;
  }[];

  /** @minItems 3 */
  /** @maxItems 3 */
  carbonEmissionVehicles?: {
    type: CarbonEmissionVehicleEnum;

    /** @minimum 0 */
    count: number;

    /** @minimum 0 */
    usage: number;
  }[];
}

export interface UpdateOneCarbonEmissionView {
  id: number;
}
