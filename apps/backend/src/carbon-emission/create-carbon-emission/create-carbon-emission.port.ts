import { AsyncStorageManager, CarbonEmissionRepository, PrimaryPort } from '@lib/application';
import { ArrayUtil, DateUtil } from '@lib/common';
import { CarbonEmissionAggregate } from '@lib/domain';
import { Transaction } from '@lib/infrastructure';
import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateCarbonEmissionData, CreateCarbonEmissionView } from './create-carbon-emission.type';

@Injectable()
export class CreateCarbonEmissionPort implements PrimaryPort {
  constructor(
    private readonly asyncStorageManager: AsyncStorageManager,
    private readonly carbonEmissionRepository: CarbonEmissionRepository,
  ) {}

  @Transaction()
  async execute(data: CreateCarbonEmissionData): Promise<CreateCarbonEmissionView> {
    const {
      startDate,
      endDate,
      carbonEmissionHeatings,
      carbonEmissionFacilities,
      carbonEmissionVehicles,
      ...etc
    } = data;

    this.validateDates(startDate, endDate);
    this.validateCarbonEmissionHeatings(carbonEmissionHeatings);
    this.validateCarbonEmissionFacilities(carbonEmissionFacilities);
    this.validateCarbonEmissionVehicles(carbonEmissionVehicles);

    const newCarbonEmission = await this.carbonEmissionRepository.saveOne(
      CarbonEmissionAggregate.create({
        id: 0,
        startDate: new Date(startDate),
        endDate: new Date(endDate),
        userId: this.asyncStorageManager.getItem('currUser').id,
        carbonEmissionHeatings,
        carbonEmissionFacilities,
        carbonEmissionVehicles,
        ...etc,
      }),
    );

    return { id: newCarbonEmission.id };
  }

  private validateDates(startDate: string, endDate: string) {
    if (!DateUtil.isBefore(startDate, endDate))
      throw new BadRequestException(`start date must be before end date.`);

    return true;
  }

  private validateCarbonEmissionHeatings(
    heatings: CreateCarbonEmissionData['carbonEmissionHeatings'],
  ) {
    const uniq = ArrayUtil.uniqueBy(heatings, (each) => each.type);
    if (uniq.length !== 1) throw new BadRequestException(`invalid carbon emission heatings.`);

    return true;
  }

  private validateCarbonEmissionFacilities(
    facilities: CreateCarbonEmissionData['carbonEmissionFacilities'],
  ) {
    const uniq = ArrayUtil.uniqueBy(facilities, (each) => each.type);
    if (uniq.length !== 3) throw new BadRequestException(`invalid carbon emission facilities.`);

    return true;
  }

  private validateCarbonEmissionVehicles(
    vehicles: CreateCarbonEmissionData['carbonEmissionVehicles'],
  ) {
    const uniq = ArrayUtil.uniqueBy(vehicles, (each) => each.type);
    if (uniq.length !== 3) throw new BadRequestException(`invalid carbon emission vehicles.`);

    return true;
  }
}
