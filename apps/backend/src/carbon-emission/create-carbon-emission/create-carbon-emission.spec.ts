import { CarbonEmissionAggregate } from '@lib/domain';
import { deepEqual, when } from 'ts-mockito';
import { CreateCarbonEmissionPort } from './create-carbon-emission.port';
import { CreateCarbonEmissionData } from './create-carbon-emission.type';
import {
  StubAsyncStorageManager,
  StubCarbonEmissionRepository,
  UnitTestBuilder,
} from '@lib/common';

describe('CreateCarbonEmissionSpec', () => {
  const {
    injector: createCarbonEmissionPort,
    stubs: [asyncStorageManager, carbonEmissionRepository],
  } = UnitTestBuilder.setInjector(CreateCarbonEmissionPort)
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    .setStubs(StubAsyncStorageManager, StubCarbonEmissionRepository)
    .build();

  test('should success to create new one', async () => {
    // given
    const data: CreateCarbonEmissionData = {
      startDate: '2009-01-26',
      endDate: '2025-09-11',
      electricityUsage: 3,
      electricityFee: 10,
      renewableEnergyPercent: 3,
      caluatedValue: 1,
      carbonEmissionHeatings: [
        {
          type: 'lng',
          usage: 9,
        },
      ],
      carbonEmissionFacilities: [
        {
          type: 'lpg',
          count: 5,
          usage: 0,
        },
        {
          type: 'diesel',
          count: 7,
          usage: 7,
        },
        {
          type: 'gasoline',
          count: 3,
          usage: 2,
        },
      ],
      carbonEmissionVehicles: [
        {
          type: 'gasoline',
          count: 1,
          usage: 2,
        },
        {
          type: 'diesel',
          count: 7,
          usage: 5,
        },
        {
          type: 'lpg',
          count: 3,
          usage: 8,
        },
      ],
    };

    const { startDate, endDate, ...etc } = data;
    when(asyncStorageManager.getItem('currUser')).thenReturn({
      id: 100,
      userVerficiations: ['email'],
      userRoles: ['member'],
    });
    when(
      carbonEmissionRepository.saveOne(
        deepEqual(
          CarbonEmissionAggregate.create({
            id: 0,
            startDate: new Date(startDate),
            endDate: new Date(endDate),
            userId: 100,
            ...etc,
          }),
        ),
      ),
    ).thenResolve(
      CarbonEmissionAggregate.create({
        id: 200,
        startDate: new Date(startDate),
        endDate: new Date(endDate),
        userId: 100,
        ...etc,
      }),
    );

    // when
    const result = await createCarbonEmissionPort.execute(data);

    // then
    expect(result.id).toEqual(200);
  });

  test('should fail to create new one. (invalid dates)', async () => {
    // given
    const data: CreateCarbonEmissionData = {
      startDate: '2025-09-11',
      endDate: '2009-01-26',
      electricityUsage: 3,
      electricityFee: 10,
      renewableEnergyPercent: 3,
      caluatedValue: 1,
      carbonEmissionHeatings: [
        {
          type: 'lng',
          usage: 9,
        },
      ],
      carbonEmissionFacilities: [
        {
          type: 'lpg',
          count: 5,
          usage: 0,
        },
        {
          type: 'diesel',
          count: 7,
          usage: 7,
        },
        {
          type: 'gasoline',
          count: 3,
          usage: 2,
        },
      ],
      carbonEmissionVehicles: [
        {
          type: 'gasoline',
          count: 1,
          usage: 2,
        },
        {
          type: 'diesel',
          count: 7,
          usage: 5,
        },
        {
          type: 'lpg',
          count: 3,
          usage: 8,
        },
      ],
    };

    const { startDate, endDate, ...etc } = data;
    when(asyncStorageManager.getItem('currUser')).thenReturn({
      id: 100,
      userVerficiations: ['email'],
      userRoles: ['member'],
    });
    when(
      carbonEmissionRepository.saveOne(
        deepEqual(
          CarbonEmissionAggregate.create({
            id: 0,
            startDate: new Date(startDate),
            endDate: new Date(endDate),
            userId: 100,
            ...etc,
          }),
        ),
      ),
    ).thenResolve(
      CarbonEmissionAggregate.create({
        id: 200,
        startDate: new Date(startDate),
        endDate: new Date(endDate),
        userId: 100,
        ...etc,
      }),
    );

    // when
    const result = (await createCarbonEmissionPort.execute(data).catch((err) => err)) as Error;

    // then
    expect(result.message).toEqual(`start date must be before end date.`);
  });

  test('should fail to create new one. (invalid heatings)', async () => {
    // given
    const data: CreateCarbonEmissionData = {
      startDate: '2009-01-26',
      endDate: '2025-09-11',
      electricityUsage: 3,
      electricityFee: 10,
      renewableEnergyPercent: 3,
      caluatedValue: 1,
      carbonEmissionHeatings: [
        {
          type: 'lng',
          usage: 9,
        },
        {
          type: 'electricity',
          usage: 9,
        },
      ],
      carbonEmissionFacilities: [
        {
          type: 'lpg',
          count: 5,
          usage: 0,
        },
        {
          type: 'diesel',
          count: 7,
          usage: 7,
        },
        {
          type: 'gasoline',
          count: 3,
          usage: 2,
        },
      ],
      carbonEmissionVehicles: [
        {
          type: 'gasoline',
          count: 1,
          usage: 2,
        },
        {
          type: 'diesel',
          count: 7,
          usage: 5,
        },
        {
          type: 'lpg',
          count: 3,
          usage: 8,
        },
      ],
    };

    const { startDate, endDate, ...etc } = data;
    when(asyncStorageManager.getItem('currUser')).thenReturn({
      id: 100,
      userVerficiations: ['email'],
      userRoles: ['member'],
    });
    when(
      carbonEmissionRepository.saveOne(
        deepEqual(
          CarbonEmissionAggregate.create({
            id: 0,
            startDate: new Date(startDate),
            endDate: new Date(endDate),
            userId: 100,
            ...etc,
          }),
        ),
      ),
    ).thenResolve(
      CarbonEmissionAggregate.create({
        id: 200,
        startDate: new Date(startDate),
        endDate: new Date(endDate),
        userId: 100,
        ...etc,
      }),
    );

    // when
    const result = (await createCarbonEmissionPort.execute(data).catch((err) => err)) as Error;

    // then
    expect(result.message).toEqual(`invalid carbon emission heatings.`);
  });

  test('should fail to create new one. (invalid facilities)', async () => {
    // given
    const data: CreateCarbonEmissionData = {
      startDate: '2009-01-26',
      endDate: '2025-09-11',
      electricityUsage: 3,
      electricityFee: 10,
      renewableEnergyPercent: 3,
      caluatedValue: 1,
      carbonEmissionHeatings: [
        {
          type: 'lng',
          usage: 9,
        },
      ],
      carbonEmissionFacilities: [
        {
          type: 'lpg',
          count: 5,
          usage: 0,
        },
        {
          type: 'diesel',
          count: 7,
          usage: 7,
        },
        {
          type: 'diesel',
          count: 3,
          usage: 2,
        },
      ],
      carbonEmissionVehicles: [
        {
          type: 'gasoline',
          count: 1,
          usage: 2,
        },
        {
          type: 'diesel',
          count: 7,
          usage: 5,
        },
        {
          type: 'lpg',
          count: 3,
          usage: 8,
        },
      ],
    };

    const { startDate, endDate, ...etc } = data;
    when(asyncStorageManager.getItem('currUser')).thenReturn({
      id: 100,
      userVerficiations: ['email'],
      userRoles: ['member'],
    });
    when(
      carbonEmissionRepository.saveOne(
        deepEqual(
          CarbonEmissionAggregate.create({
            id: 0,
            startDate: new Date(startDate),
            endDate: new Date(endDate),
            userId: 100,
            ...etc,
          }),
        ),
      ),
    ).thenResolve(
      CarbonEmissionAggregate.create({
        id: 200,
        startDate: new Date(startDate),
        endDate: new Date(endDate),
        userId: 100,
        ...etc,
      }),
    );

    // when
    const result = (await createCarbonEmissionPort.execute(data).catch((err) => err)) as Error;

    // then
    expect(result.message).toEqual(`invalid carbon emission facilities.`);
  });

  test('should fail to create new one. (invalid vehicles)', async () => {
    // given
    const data: CreateCarbonEmissionData = {
      startDate: '2009-01-26',
      endDate: '2025-09-11',
      electricityUsage: 3,
      electricityFee: 10,
      renewableEnergyPercent: 3,
      caluatedValue: 1,
      carbonEmissionHeatings: [
        {
          type: 'lng',
          usage: 9,
        },
      ],
      carbonEmissionFacilities: [
        {
          type: 'lpg',
          count: 5,
          usage: 0,
        },
        {
          type: 'diesel',
          count: 7,
          usage: 7,
        },
        {
          type: 'gasoline',
          count: 3,
          usage: 2,
        },
      ],
      carbonEmissionVehicles: [
        {
          type: 'gasoline',
          count: 1,
          usage: 2,
        },
        {
          type: 'diesel',
          count: 7,
          usage: 5,
        },
        {
          type: 'diesel',
          count: 3,
          usage: 8,
        },
      ],
    };

    const { startDate, endDate, ...etc } = data;
    when(asyncStorageManager.getItem('currUser')).thenReturn({
      id: 100,
      userVerficiations: ['email'],
      userRoles: ['member'],
    });
    when(
      carbonEmissionRepository.saveOne(
        deepEqual(
          CarbonEmissionAggregate.create({
            id: 0,
            startDate: new Date(startDate),
            endDate: new Date(endDate),
            userId: 100,
            ...etc,
          }),
        ),
      ),
    ).thenResolve(
      CarbonEmissionAggregate.create({
        id: 200,
        startDate: new Date(startDate),
        endDate: new Date(endDate),
        userId: 100,
        ...etc,
      }),
    );

    // when
    const result = (await createCarbonEmissionPort.execute(data).catch((err) => err)) as Error;

    // then
    expect(result.message).toEqual(`invalid carbon emission vehicles.`);
  });
});
