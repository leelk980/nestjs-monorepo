import { AsyncStorageManager, CarbonEmissionRepository, PrimaryPort } from '@lib/application';
import { Transaction } from '@lib/infrastructure';
import { ForbiddenException, Injectable } from '@nestjs/common';
import {
  DeleteOneCarbonEmissionData,
  DeleteOneCarbonEmissionView,
} from './delete-one-carbon-emission.type';

@Injectable()
export class DeleteOneCarbonEmissionPort implements PrimaryPort {
  constructor(
    private readonly asyncStorageManager: AsyncStorageManager,
    private readonly carbonEmissionRepository: CarbonEmissionRepository,
  ) {}

  @Transaction()
  async execute(data: DeleteOneCarbonEmissionData): Promise<DeleteOneCarbonEmissionView> {
    const { id } = data;

    const carbonEmissionAgg = await this.carbonEmissionRepository.findOneById(id);

    this.checkOwnerPermission(carbonEmissionAgg.userId);

    await this.carbonEmissionRepository.softDeleteOne(carbonEmissionAgg);

    return { id };
  }

  private checkOwnerPermission(ownerId: number) {
    const currUser = this.asyncStorageManager.getItem('currUser');
    if (currUser.id !== ownerId) {
      throw new ForbiddenException(`no permission for deleting carbon emission. ${currUser.id}`);
    }

    return true;
  }
}
