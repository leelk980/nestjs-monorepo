export interface DeleteOneCarbonEmissionData {
  /** @minimum 0 */
  id: number;
}

export interface DeleteOneCarbonEmissionView {
  id: number;
}
