import { Pagination } from '@lib/common';

export interface FindManyCarbonEmissionsData {
  /** @minimum 0 */
  userId: number;

  /** @minimum 0 */
  index: number;

  /** @minimum 0 */
  /** @maximum 100 */
  size: number;

  sort: 'recent' | 'popular';
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface FindManyCarbonEmissionsView
  extends Pagination<{
    id: number;
    startDate: string;
    endDate: string;
  }> {}
