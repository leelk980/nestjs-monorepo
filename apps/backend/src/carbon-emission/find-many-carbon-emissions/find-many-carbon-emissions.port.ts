import { AsyncStorageManager, CarbonEmissionRepository, PrimaryPort } from '@lib/application';
import { DateUtil } from '@lib/common';
import { Transaction } from '@lib/infrastructure';
import { ForbiddenException, Injectable } from '@nestjs/common';
import {
  FindManyCarbonEmissionsData,
  FindManyCarbonEmissionsView,
} from './find-many-carbon-emissions.type';

@Injectable()
export class FindManyCarbonEmissionsPort implements PrimaryPort {
  constructor(
    private readonly asyncStorageManager: AsyncStorageManager,
    private readonly carbonEmissionRepository: CarbonEmissionRepository,
  ) {}

  @Transaction()
  async execute(data: FindManyCarbonEmissionsData): Promise<FindManyCarbonEmissionsView> {
    const { userId, index, size } = data;

    this.checkOwnerPermission(userId);

    const piece = await this.carbonEmissionRepository.findMany({
      filter: { userId },
      page: { index, size },
    });

    return {
      data: piece.data.map((each) => ({
        id: each.id,
        startDate: DateUtil.toDateString(each.startDate),
        endDate: DateUtil.toDateString(each.endDate),
      })),
      page: piece.page,
    };
  }

  private checkOwnerPermission(ownerId: number) {
    const currUser = this.asyncStorageManager.getItem('currUser');
    if (currUser.id !== ownerId) {
      throw new ForbiddenException(`no permission for finding carbon emissions. ${currUser.id}`);
    }

    return true;
  }
}
