import { Optional } from '@lib/common';
import { Auth } from '@lib/infrastructure';
import { TypedBody, TypedParam, TypedQuery, TypedRoute } from '@nestia/core';
import { Controller } from '@nestjs/common';
import { FindOneCarbonEmissionPort, FindOneCarbonEmissionView } from './find-one-carbon-emission';
import {
  DeleteOneCarbonEmissionPort,
  DeleteOneCarbonEmissionView,
} from './delete-one-carbon-emission';
import {
  UpdateOneCarbonEmissionData,
  UpdateOneCarbonEmissionPort,
  UpdateOneCarbonEmissionView,
} from './update-one-carbon-emission';
import {
  FindManyCarbonEmissionsData,
  FindManyCarbonEmissionsPort,
  FindManyCarbonEmissionsView,
} from './find-many-carbon-emissions';
import {
  CreateCarbonEmissionData,
  CreateCarbonEmissionPort,
  CreateCarbonEmissionView,
} from './create-carbon-emission';

@Controller('/carbon-emissions')
export class CarbonEmissionController {
  constructor(
    private readonly createCarbonEmissionPort: CreateCarbonEmissionPort,
    private readonly updateOneCarbonEmissionPort: UpdateOneCarbonEmissionPort,
    private readonly deleteOneCarbonEmissionPort: DeleteOneCarbonEmissionPort,
    private readonly findManyCarbonEmissionsPort: FindManyCarbonEmissionsPort,
    private readonly findOneCarbonEmissionPort: FindOneCarbonEmissionPort,
  ) {}

  /**
   * 탄소 배출량 측정하기.
   *
   * @tag CarbonEmission / Member
   *
   * @param data 필요한 데이터
   *
   * @returns 탄소 배출 아이디
   */
  @Auth('member')
  @TypedRoute.Post('/')
  async createCarbonEmission(
    @TypedBody() data: CreateCarbonEmissionData,
  ): Promise<CreateCarbonEmissionView> {
    return this.createCarbonEmissionPort.execute(data);
  }

  /**
   * 탄소 배출량 수정하기.
   *
   * @tag CarbonEmission / Member
   *
   * @param id 탄소 배출 아이디
   *
   * @param data 필요한 데이터
   *
   * @returns 탄소 배출 아이디
   */
  @Auth('member')
  @TypedRoute.Patch('/:id')
  async updateOneCarbonEmission(
    @TypedParam('id') id: number,
    @TypedBody() data: Omit<Optional<UpdateOneCarbonEmissionData>, 'id'>, // @todo Optional 안 쓰고 싶음...
  ): Promise<UpdateOneCarbonEmissionView> {
    return this.updateOneCarbonEmissionPort.execute({ id, ...data } as UpdateOneCarbonEmissionData);
  }

  /**
   * 탄소 배출량 삭제하기.
   *
   * @tag CarbonEmission / Member
   *
   * @param id 탄소 배출 아이디
   *
   * @returns 탄소 배출 아이디
   */
  @Auth('member')
  @TypedRoute.Delete('/:id')
  async deleteOneCarbonEmission(
    @TypedParam('id') id: number,
  ): Promise<DeleteOneCarbonEmissionView> {
    return this.deleteOneCarbonEmissionPort.execute({ id });
  }

  /**
   * 탄소 배출량 측정 기록 리스트 조회하기.
   *
   * @tag CarbonEmission / Member
   *
   * @param query 필터(userId), 페이지(index, size), 정렬(sort) 조건
   *
   * @returns 페이지네이션
   */
  @Auth('member')
  @TypedRoute.Get('/')
  async findManyCarbonEmissions(
    @TypedQuery() query: FindManyCarbonEmissionsData,
  ): Promise<FindManyCarbonEmissionsView> {
    return this.findManyCarbonEmissionsPort.execute(query);
  }

  /**
   * 단일 탄소 배출량 측정 기록 조회하기.
   *
   * @tag CarbonEmission / Member
   *
   * @param id 아이디
   *
   * @returns 상세 정보
   */
  @Auth('member')
  @TypedRoute.Get('/:id')
  async findOneCarbonEmission(@TypedParam('id') id: number): Promise<FindOneCarbonEmissionView> {
    return this.findOneCarbonEmissionPort.execute({ id });
  }
}
