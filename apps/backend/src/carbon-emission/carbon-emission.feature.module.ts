import { AsyncStorageManager, CarbonEmissionRepository } from '@lib/application';
import { ManagerInfraModule, RepositoryInfraModule } from '@lib/infrastructure';
import { Module } from '@nestjs/common';
import { CarbonEmissionController } from './carbon-emission.controller';
import { CreateCarbonEmissionPort } from './create-carbon-emission';
import { DeleteOneCarbonEmissionPort } from './delete-one-carbon-emission';
import { FindManyCarbonEmissionsPort } from './find-many-carbon-emissions';
import { FindOneCarbonEmissionPort } from './find-one-carbon-emission';
import { UpdateOneCarbonEmissionPort } from './update-one-carbon-emission';

@Module({
  imports: [
    ManagerInfraModule.forFeature(AsyncStorageManager),
    RepositoryInfraModule.forFeature(CarbonEmissionRepository),
  ],
  controllers: [CarbonEmissionController],
  providers: [
    CreateCarbonEmissionPort,
    UpdateOneCarbonEmissionPort,
    DeleteOneCarbonEmissionPort,
    FindManyCarbonEmissionsPort,
    FindOneCarbonEmissionPort,
  ],
})
export class CarbonEmissionFeatureModule {}
