import { AsyncStorageManager, CarbonEmissionRepository, PrimaryPort } from '@lib/application';
import { DateUtil } from '@lib/common';
import { Transaction } from '@lib/infrastructure';
import { ForbiddenException, Injectable } from '@nestjs/common';
import {
  FindOneCarbonEmissionData,
  FindOneCarbonEmissionView,
} from './find-one-carbon-emission.type';

@Injectable()
export class FindOneCarbonEmissionPort implements PrimaryPort {
  constructor(
    private readonly asyncStorageManager: AsyncStorageManager,
    private readonly carbonEmissionRepository: CarbonEmissionRepository,
  ) {}

  @Transaction()
  async execute(data: FindOneCarbonEmissionData): Promise<FindOneCarbonEmissionView> {
    const { startDate, endDate, ...etc } = await this.carbonEmissionRepository.findOneById(data.id);

    this.checkOwnerPermission(etc.userId);

    return {
      startDate: DateUtil.toDateString(startDate),
      endDate: DateUtil.toDateString(endDate),
      ...etc,
    };
  }

  private checkOwnerPermission(ownerId: number) {
    const currUser = this.asyncStorageManager.getItem('currUser');
    if (currUser.id !== ownerId) {
      throw new ForbiddenException(`no permission for finding carbon emission. ${currUser.id}`);
    }

    return true;
  }
}
