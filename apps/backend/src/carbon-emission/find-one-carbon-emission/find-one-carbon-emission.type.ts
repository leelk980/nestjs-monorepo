import {
  CarbonEmissionFaciltityEnum,
  CarbonEmissionHeatingEnum,
  CarbonEmissionVehicleEnum,
} from '@lib/common';

export interface FindOneCarbonEmissionData {
  /** @minimum 0 */
  id: number;
}

export interface FindOneCarbonEmissionView {
  id: number;
  startDate: string;
  endDate: string;
  electricityUsage: number;
  electricityFee: number;
  caluatedValue: number;
  renewableEnergyPercent: number;
  userId: number;
  carbonEmissionHeatings: {
    type: CarbonEmissionHeatingEnum;
    usage: number;
  }[];
  carbonEmissionFacilities: {
    type: CarbonEmissionFaciltityEnum;
    count: number;
    usage: number;
  }[];
  carbonEmissionVehicles: {
    type: CarbonEmissionVehicleEnum;
    count: number;
    usage: number;
  }[];
}
