import { Auth } from '@lib/infrastructure';
import { TypedBody, TypedRoute } from '@nestia/core';
import { Controller } from '@nestjs/common';
import { NoneRestService } from './none-rest.service';
import { GeneratePresignedUrlForPutData, GeneratePresignedUrlForPutView } from './none-rest.type';

@Controller('/none-rest')
export class NoneRestController {
  constructor(private readonly commonService: NoneRestService) {}

  /**
   * health check api
   *
   * @internal
   */
  @Auth('public')
  @TypedRoute.Get('/health')
  async checkHealth(): Promise<string> {
    return 'ok!';
  }

  /**
   * s3 업로드를 위한 Presigned url 생성하기.
   *
   * @tag NoneRest / Member
   *
   * @param data 필요한 데이터
   *
   * @return presigned-url
   */
  @Auth('member')
  @TypedRoute.Put('/presigned-url')
  async generatePresignedUrlForPut(
    @TypedBody() data: GeneratePresignedUrlForPutData,
  ): Promise<GeneratePresignedUrlForPutView> {
    return this.commonService.generatePresignedUrlForPut(data);
  }
}
