import { UserFileEnum } from '@lib/common';

export interface GeneratePresignedUrlForPutData {
  type: UserFileEnum;
}

export interface GeneratePresignedUrlForPutView {
  presignedUrl: string;
}
