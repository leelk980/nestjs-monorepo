import { FileStorageManager } from '@lib/application';
import { Injectable } from '@nestjs/common';
import { GeneratePresignedUrlForPutData, GeneratePresignedUrlForPutView } from './none-rest.type';

@Injectable()
export class NoneRestService {
  constructor(private readonly fileStorageManager: FileStorageManager) {}

  async generatePresignedUrlForPut(
    data: GeneratePresignedUrlForPutData,
  ): Promise<GeneratePresignedUrlForPutView> {
    const { type } = data;
    const presignedUrl = await this.fileStorageManager.generatePresignedUrlForPut(
      `tmp/${type}/${Date.now()}`,
    );

    return { presignedUrl };
  }
}
