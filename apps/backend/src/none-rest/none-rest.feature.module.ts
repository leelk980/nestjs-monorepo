import { AsyncStorageManager, FileStorageManager } from '@lib/application';
import { ManagerInfraModule } from '@lib/infrastructure';
import { Module } from '@nestjs/common';
import { NoneRestController } from './none-rest.controller';
import { NoneRestService } from './none-rest.service';

@Module({
  imports: [ManagerInfraModule.forFeature(AsyncStorageManager, FileStorageManager)],
  controllers: [NoneRestController],
  providers: [NoneRestService],
})
export class NoneRestFeatureModule {}
