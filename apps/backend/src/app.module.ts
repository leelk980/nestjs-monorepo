import { environment } from '@lib/common';
import { Module } from '@nestjs/common';
import { CarbonEmissionFeatureModule } from './carbon-emission/carbon-emission.feature.module';
import { NoneRestFeatureModule } from './none-rest/none-rest.feature.module';
import { UserFeatureModule } from './user/user.feature.module';
import {
  ManagerInfraModule,
  MiddlewareInfraModule,
  RepositoryInfraModule,
} from '@lib/infrastructure';

@Module({
  imports: [
    ManagerInfraModule.forRoot({
      asyncStorage: true,
      fileStorage: true,
      hash: true,
      jwt: true,
    }),
    MiddlewareInfraModule.forRoot({
      guard: true,
      interceptor: true,
      filter: true,
    }),
    RepositoryInfraModule.forRoot({
      type: 'mysql',
      host: environment.database.host,
      port: environment.database.port,
      username: environment.database.username,
      password: environment.database.password,
      name: environment.database.name,
    }),
    NoneRestFeatureModule,
    UserFeatureModule,
    CarbonEmissionFeatureModule,
  ],
})
export class AppModule {}
