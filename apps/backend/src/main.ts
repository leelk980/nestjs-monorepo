import { NestFactory } from '@nestjs/core';
import { SwaggerModule } from '@nestjs/swagger';
import { join } from 'path';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const docs = require(join(__dirname, '../../../../packages/backend-sdk/swagger.json'));
  docs.servers = [{ url: 'http://localhost:3000' }];
  docs.info = {
    title: 'renew-us-backend',
    description: 'renew-us-backend api documents.',
  };
  SwaggerModule.setup('docs', app, docs);

  await app.listen(3000);

  console.log(`Backend is running on 3000`);
}

bootstrap();
