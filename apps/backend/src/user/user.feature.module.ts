import { ManagerInfraModule, RepositoryInfraModule } from '@lib/infrastructure';
import { Module } from '@nestjs/common';
import { GetUserMePort } from './get-user-me';
import { SignInUserPort } from './sign-in-user';
import { SignUpUserPort } from './sign-up-user';
import { UpdateUserByIdPort } from './update-user-by-id';
import { UserController } from './user.controller';
import { VerifyUserByIdPort } from './verify-user-by-id';
import {
  AsyncStorageManager,
  CompanyRepository,
  HashManager,
  JwtManager,
  UserRepository,
} from '@lib/application';

@Module({
  imports: [
    ManagerInfraModule.forFeature(AsyncStorageManager, HashManager, JwtManager),
    RepositoryInfraModule.forFeature(UserRepository, CompanyRepository),
  ],
  controllers: [UserController],
  providers: [
    SignUpUserPort,
    SignInUserPort,
    UpdateUserByIdPort,
    VerifyUserByIdPort,
    GetUserMePort,
  ],
})
export class UserFeatureModule {}
