import { UserAggregate } from '@lib/domain';
import { capture, when } from 'ts-mockito';
import { random } from 'typia';
import { VerifyUserByIdPort } from './verify-user-by-id.port';
import { VerifyUserByIdData } from './verify-user-by-id.type';
import {
  DateUtil,
  StubAsyncStorageManager,
  StubUserRepository,
  UnitTestBuilder,
} from '@lib/common';

describe('VerifyUserById', () => {
  const {
    injector: verifyUserByIdPort,
    stubs: [asyncStorageManager, userRepository],
  } = UnitTestBuilder.setInjector(VerifyUserByIdPort)
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    .setStubs(StubAsyncStorageManager, StubUserRepository)
    .build();

  test('should success to create new user verification', async () => {
    // given
    const data: VerifyUserByIdData = {
      id: 100,
      type: 'email',
      value: 'test2@test.com',
    };
    when(asyncStorageManager.getItem('currUser')).thenReturn({
      id: 100,
      userVerficiations: ['email'],
      userRoles: ['member'],
    });
    when(userRepository.findOneByEmailOrNull('test@test.com')).thenResolve(null);
    when(userRepository.findOneById(100)).thenResolve(
      UserAggregate.create({
        ...random<UserAggregate>(),
        id: 100,
        userVerfications: [
          {
            type: 'email',
            value: 'test1@test.com',
            isVerified: false,
            sentAt: new Date(),
            verificationCode: '123456',
          },
        ],
      }),
    );

    // when
    await verifyUserByIdPort.execute(data);
    const captured = capture(userRepository.saveOne).last()[0];

    // then
    expect(captured.userVerfications.length).toEqual(2);
    expect(captured.userVerfications[1]?.type).toEqual('email');
    expect(captured.userVerfications[1]?.value).toEqual('test2@test.com');
    expect(captured.userVerfications[1]?.verificationCode).toBeTruthy();
    expect(captured.userVerfications[1]?.isVerified).toEqual(false);
    expect(captured.userVerfications[1]?.sentAt).toEqual(null);
  });

  test('should success to renew user verification', async () => {
    // given
    const data: VerifyUserByIdData = {
      id: 100,
      type: 'email',
      value: 'test@test.com',
    };
    when(asyncStorageManager.getItem('currUser')).thenReturn({
      id: 100,
      userVerficiations: ['email'],
      userRoles: ['member'],
    });
    when(userRepository.findOneByEmailOrNull('test@test.com')).thenResolve(null);
    when(userRepository.findOneById(100)).thenResolve(
      UserAggregate.create({
        ...random<UserAggregate>(),
        id: 100,
        userVerfications: [
          {
            type: 'email',
            value: 'test@test.com',
            isVerified: false,
            sentAt: DateUtil.minusMinute(new Date(), 1000),
            verificationCode: '123456',
          },
        ],
      }),
    );

    // when
    await verifyUserByIdPort.execute(data);
    const captured = capture(userRepository.saveOne).last()[0];

    // then
    expect(captured.userVerfications.length).toEqual(1);
    expect(captured.userVerfications[0]?.type).toEqual('email');
    expect(captured.userVerfications[0]?.value).toEqual('test@test.com');
    expect(captured.userVerfications[0]?.verificationCode).toBeTruthy();
    expect(captured.userVerfications[0]?.isVerified).not.toEqual('123456');
    expect(captured.userVerfications[0]?.sentAt).toEqual(null);
  });

  test('should success to verify user verification (same email)', async () => {
    // given
    const data: VerifyUserByIdData = {
      id: 100,
      type: 'email',
      value: 'test@test.com',
      verificationCode: '123456',
    };
    when(asyncStorageManager.getItem('currUser')).thenReturn({
      id: 100,
      userVerficiations: ['email'],
      userRoles: ['member'],
    });
    when(userRepository.findOneByEmailOrNull('test@test.com')).thenResolve(null);
    when(userRepository.findOneById(100)).thenResolve(
      UserAggregate.create({
        ...random<UserAggregate>(),
        id: 100,
        userVerfications: [
          {
            type: 'email',
            value: 'test@test.com',
            verificationCode: '123456',
            isVerified: false,
            sentAt: new Date(),
          },
        ],
      }),
    );

    // when
    await verifyUserByIdPort.execute(data);
    const captured = capture(userRepository.saveOne).last()[0];

    // then
    expect(captured.email).toEqual('test@test.com');
    expect(captured.userVerfications[0]?.type).toEqual('email');
    expect(captured.userVerfications[0]?.value).toEqual('test@test.com');
    expect(captured.userVerfications[0]?.verificationCode).toEqual('123456');
    expect(captured.userVerfications[0]?.isVerified).toEqual(true);
    expect(captured.userVerfications[0]?.sentAt).toBeTruthy();
  });

  test('should success to verify user verification (different email)', async () => {
    // given
    const data: VerifyUserByIdData = {
      id: 100,
      type: 'email',
      value: 'diff@test.com',
      verificationCode: '123456',
    };
    when(asyncStorageManager.getItem('currUser')).thenReturn({
      id: 100,
      userVerficiations: ['email'],
      userRoles: ['member'],
    });
    when(userRepository.findOneByEmailOrNull('test@test.com')).thenResolve(null);
    when(userRepository.findOneById(100)).thenResolve(
      UserAggregate.create({
        ...random<UserAggregate>(),
        id: 100,
        userVerfications: [
          {
            type: 'email',
            value: 'diff@test.com',
            verificationCode: '123456',
            isVerified: false,
            sentAt: new Date(),
          },
        ],
      }),
    );

    // when
    await verifyUserByIdPort.execute(data);
    const captured = capture(userRepository.saveOne).last()[0];

    // then
    expect(captured.email).toEqual('diff@test.com');
    expect(captured.userVerfications[0]?.type).toEqual('email');
    expect(captured.userVerfications[0]?.value).toEqual('diff@test.com');
    expect(captured.userVerfications[0]?.verificationCode).toEqual('123456');
    expect(captured.userVerfications[0]?.isVerified).toEqual(true);
    expect(captured.userVerfications[0]?.sentAt).toBeTruthy();
  });
});
