import { AsyncStorageManager, PrimaryPort, UserRepository } from '@lib/application';
import { emailPattern, phonePattern } from '@lib/common';
import { Transaction } from '@lib/infrastructure';
import { BadRequestException, ForbiddenException, Injectable } from '@nestjs/common';
import { VerifyUserByIdData, VerifyUserByIdView } from './verify-user-by-id.type';

@Injectable()
export class VerifyUserByIdPort implements PrimaryPort {
  constructor(
    private readonly asyncStorageManager: AsyncStorageManager,
    private readonly userRepository: UserRepository,
  ) {}

  @Transaction()
  async execute(data: VerifyUserByIdData): Promise<VerifyUserByIdView> {
    const { id, type, value, verificationCode } = data;

    await this.checkOwnerPermission(id);
    await this.validateTypeAndValue({ type, value });

    const userAgg = await this.userRepository.findOneById(id);

    const result = (() => {
      if (verificationCode) {
        return userAgg.approveUserVerification({ type, value, verificationCode });
      } else {
        return userAgg.upsertUserVerfication({
          type,
          value,
          verificationCode: (Math.floor(Math.random() * 900000) + 100000).toString(),
          isVerified: false,
          sentAt: null,
        });
      }
    })();

    if (result instanceof Error) throw new BadRequestException(result.message);

    await this.userRepository.saveOne(userAgg);

    return { id: userAgg.id };
  }

  private async checkOwnerPermission(ownerId: number) {
    const currUser = this.asyncStorageManager.getItem('currUser');

    if (currUser.id !== ownerId) {
      throw new ForbiddenException(`no permission for verifying user. ${ownerId}.`);
    }
  }

  private async validateTypeAndValue(params: Pick<VerifyUserByIdData, 'type' | 'value'>) {
    const currUser = this.asyncStorageManager.getItem('currUser');
    const { type, value } = params;

    if (type === 'email') {
      if (!emailPattern.test(value)) {
        throw new BadRequestException(`invalid ${type} format. ${value}.`);
      }

      const userAgg = await this.userRepository.findOneByEmailOrNull(value);
      if (userAgg && userAgg.id !== currUser.id) {
        throw new BadRequestException(`${type} already exists.`);
      }
    }

    if (type === 'phone') {
      if (!phonePattern.test(value)) {
        throw new BadRequestException(`invalid ${type} format. ${value}.`);
      }

      const userAgg = await this.userRepository.findOneByPhoneOrNull(value);
      if (userAgg && userAgg.id !== currUser.id) {
        throw new BadRequestException(`${type} already exists.`);
      }
    }
  }
}
