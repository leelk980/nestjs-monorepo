import { Nullable, UserVerificationEnum } from '@lib/common';

export interface VerifyUserByIdData {
  id: number;
  type: UserVerificationEnum;
  value: string;
  verificationCode?: Nullable<string>;
}

export interface VerifyUserByIdView {
  id: number;
}
