import { HashManager, JwtManager, PrimaryPort, UserRepository } from '@lib/application';
import { Transaction } from '@lib/infrastructure';
import { BadRequestException, Injectable } from '@nestjs/common';
import { SignInUserData, SignInUserView } from './sign-in-user.type';

@Injectable()
export class SignInUserPort implements PrimaryPort {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly hashManager: HashManager,
    private readonly jwtManager: JwtManager,
  ) {}

  @Transaction()
  async execute(data: SignInUserData): Promise<SignInUserView> {
    const { email, password } = data;

    const userAgg = await this.userRepository.findOneByEmailOrNull(email);
    if (!userAgg || !(await this.hashManager.validateHash(password, userAgg.password))) {
      throw new BadRequestException('invalid email or password');
    }

    return {
      accessToken: this.jwtManager.encode({
        type: 'access',
        userId: userAgg.id,
        userVerficiations: userAgg.getUserVerifiedTypes(),
        userRoles: userAgg.userRoles,
      }),
      refreshToken: this.jwtManager.encode({
        type: 'refresh',
        userId: userAgg.id,
        test: 'test string',
      }),
    };
  }
}
