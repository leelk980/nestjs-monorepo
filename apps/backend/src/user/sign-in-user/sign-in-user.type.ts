export interface SignInUserData {
  /** @format email */
  email: string;

  /** @pattern ^(?=.*[!@#$%^&*()_\-+=|\\{}[\]:;"'<>,.?/~`])((?=.*\d)(?=.*[a-zA-Z])).{8,20}$ */
  password: string;
}

export interface SignInUserView {
  accessToken: string;
  refreshToken: string;
}
