import { Optional } from '@lib/common';
import { Auth } from '@lib/infrastructure';
import { TypedBody, TypedParam, TypedRoute } from '@nestia/core';
import { Controller } from '@nestjs/common';
import { GetUserMePort, GetUserMeView } from './get-user-me';
import { SignInUserData, SignInUserPort, SignInUserView } from './sign-in-user';
import { SignUpUserData, SignUpUserPort, SignUpUserView } from './sign-up-user';
import { UpdateUserByIdData, UpdateUserByIdPort, UpdateUserByIdView } from './update-user-by-id';
import { VerifyUserByIdData, VerifyUserByIdPort, VerifyUserByIdView } from './verify-user-by-id';

@Controller('/users')
export class UserController {
  constructor(
    private readonly signUpUserPort: SignUpUserPort,
    private readonly signInUserPort: SignInUserPort,
    private readonly getUserMePort: GetUserMePort,
    private readonly updateUserByIdPort: UpdateUserByIdPort,
    private readonly verifyUserByIdPort: VerifyUserByIdPort,
  ) {}

  /**
   * 회원가입하기.
   *
   * @tag User / Public
   *
   * @param data 회원가입에 필요한 정보
   *
   * @returns 유저 아이디
   */
  @Auth('public')
  @TypedRoute.Post('/sign-up')
  async signUpUser(@TypedBody() data: SignUpUserData): Promise<SignUpUserView> {
    return this.signUpUserPort.execute(data);
  }

  /**
   * 로그인하기.
   *
   * @tag User / Public
   *
   * @param data 이메일, 비밀번호
   *
   * @returns 엑세스, 리프레시 토큰
   */
  @Auth('public')
  @TypedRoute.Post('/sign-in')
  async signInUser(@TypedBody() data: SignInUserData): Promise<SignInUserView> {
    return this.signInUserPort.execute(data);
  }

  /**
   * 자기 정보 가져오기.
   *
   * @tag User / Member
   *
   * @returns 유저 정보
   */
  @Auth('member')
  @TypedRoute.Get('/me')
  async getUserMe(): Promise<GetUserMeView> {
    return this.getUserMePort.execute();
  }

  /**
   * 유저 정보 수정하기.
   *
   * 본인의 정보만 수정 가능합니다.
   *
   * @param id 유저 아이디
   *
   * @param data 수정할 내용
   *
   * @tag User / Member
   *
   * @returns 유저 정보
   */
  @Auth('member')
  @TypedRoute.Patch('/:id')
  async updateUserById(
    @TypedParam('id') id: number,
    @TypedBody() data: Omit<Optional<UpdateUserByIdData>, 'id'>, // @todo Optional 안 쓰고 싶음...
  ): Promise<UpdateUserByIdView> {
    return this.updateUserByIdPort.execute({ id, ...data } as UpdateUserByIdData);
  }

  /**
   * 유저 이메일/휴대폰 인증하기.
   *
   * @param id 유저 아이디
   *
   * @param data verificationCode 미포함: 인증코드 발송, 포함: 인증코드 확인
   *
   * @tag User / Member
   *
   * @returns 유저 아이디
   */
  @Auth('member')
  @TypedRoute.Put('/verify/:id')
  async verifyUserById(
    @TypedParam('id') id: number,
    @TypedBody() data: Omit<VerifyUserByIdData, 'id'>,
  ): Promise<VerifyUserByIdView> {
    return this.verifyUserByIdPort.execute({ id, ...data });
  }
}
