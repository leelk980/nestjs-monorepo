import { NotNullable, OmitProperties } from '@lib/common';
import { Transaction } from '@lib/infrastructure';
import { ForbiddenException, Injectable } from '@nestjs/common';
import { UpdateUserByIdData, UpdateUserByIdView } from './update-user-by-id.type';
import {
  AsyncStorageManager,
  CompanyRepository,
  PrimaryPort,
  UserRepository,
} from '@lib/application';

@Injectable()
export class UpdateUserByIdPort implements PrimaryPort {
  constructor(
    private readonly asyncStorageManager: AsyncStorageManager,
    private readonly userRepository: UserRepository,
    private readonly companyRepository: CompanyRepository,
  ) {}

  @Transaction()
  async execute(data: UpdateUserByIdData): Promise<UpdateUserByIdView> {
    const { id, company, ...etc } = data;

    await this.checkOwnerPermission(id);

    const userAgg = await this.updateUser(id, etc);
    if (company) await this.updateCompany(userAgg.companyId, company);

    return { id };
  }

  private async checkOwnerPermission(ownerId: number) {
    const currUser = this.asyncStorageManager.getItem('currUser');

    if (currUser.id !== ownerId) {
      throw new ForbiddenException(`no permission for updating user. ${ownerId}.`);
    }
  }

  private async updateUser(
    userId: number,
    data: OmitProperties<UpdateUserByIdData, 'id' | 'company'>,
  ) {
    const userAgg = await this.userRepository.findOneById(userId);
    userAgg.update(data);
    return this.userRepository.saveOne(userAgg);
  }

  private async updateCompany(companyId: number, data: NotNullable<UpdateUserByIdData['company']>) {
    const companyAgg = await this.companyRepository.findOneById(companyId);
    companyAgg.update(data);
    return this.companyRepository.saveOne(companyAgg);
  }
}
