import { CompanyBusinessTypeEnum, CompanyEmployeeSizeEnum } from '@lib/common';

export interface UpdateUserByIdData {
  /** @minimum 0 */
  id: number;

  /** @minLength 1 */
  name?: string;

  /** @minLength 1 */
  jobTitle?: string;

  company?: {
    /** @minLength 1 */
    name?: string;

    /** @minLength 1 */
    address?: string;

    /** @minimum 0 */
    annualSales?: number;

    companyBusinessType?: CompanyBusinessTypeEnum;

    companyEmployeeSize?: CompanyEmployeeSizeEnum;
  };
}

export interface UpdateUserByIdView {
  id: number;
}
