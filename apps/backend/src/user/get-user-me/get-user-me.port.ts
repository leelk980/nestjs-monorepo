import { Transaction } from '@lib/infrastructure';
import { Injectable } from '@nestjs/common';
import { GetUserMeView } from './get-user-me.type';
import {
  AsyncStorageManager,
  CompanyRepository,
  PrimaryPort,
  UserRepository,
} from '@lib/application';

@Injectable()
export class GetUserMePort implements PrimaryPort {
  constructor(
    private readonly asyncStorageManager: AsyncStorageManager,
    private readonly userRepository: UserRepository,
    private readonly companyRepository: CompanyRepository,
  ) {}

  @Transaction()
  async execute(): Promise<GetUserMeView> {
    const currUser = this.asyncStorageManager.getItem('currUser');
    const userAgg = await this.userRepository.findOneById(currUser.id);
    const companyAgg = await this.companyRepository.findOneById(userAgg.companyId);

    return {
      ...userAgg,
      userVerifications: userAgg.getUserVerifiedTypes(),
      company: companyAgg,
    };
  }
}
