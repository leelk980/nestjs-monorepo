import {
  CompanyBusinessTypeEnum,
  CompanyEmployeeSizeEnum,
  UserRoleEnum,
  UserVerificationEnum,
} from '@lib/common';

export interface GetUserMeView {
  id: number;
  email: string;
  name: string;
  jobTitle: string;
  phone: string;
  userVerifications: UserVerificationEnum[];
  userRoles: UserRoleEnum[];

  company: {
    id: number;
    name: string;
    address: string;
    phone: string;
    annualSales: number;
    companyBusinessType: CompanyBusinessTypeEnum;
    companyEmployeeSize: CompanyEmployeeSizeEnum;
  };
}
