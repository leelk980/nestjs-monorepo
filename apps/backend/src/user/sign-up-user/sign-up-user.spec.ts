import { CompanyAggregate, UserAggregate } from '@lib/domain';
import { deepEqual, when } from 'ts-mockito';
import { SignUpUserPort } from './sign-up-user.port';
import { SignUpUserData } from './sign-up-user.type';

import {
  StubCompanyRepository,
  StubHashManager,
  StubUserRepository,
  UnitTestBuilder,
} from '@lib/common';

describe('SignUpUserSpec', () => {
  const {
    injector: signUpUserPort,
    stubs: [userRepository, companyRepository, hashManager],
  } = UnitTestBuilder.setInjector(SignUpUserPort)
    .setStubs(StubUserRepository, StubCompanyRepository, StubHashManager)
    .build();

  it('if userTermses uniq length === 4, success', async () => {
    // given
    const data: SignUpUserData = {
      email: 'test3@test.com',
      password: 'qwer1234!',
      name: '테스트',
      jobTitle: '대리',
      phone: '010-1234-1236',
      userTermses: [
        'privacy_policy',
        'sensitive_information',
        'terms_of_use',
        'unique_identification_information',
      ],
      company: {
        name: '테스트회사',
        address: '서울특별시 관악구',
        phone: '010-1234-1234',
        annualSales: 7,
        companyBusinessType: 'primary',
        companyEmployeeSize: 'between_100_200',
      },
    };

    when(hashManager.generateHash(data.password, 12)).thenResolve('$hashed_password');
    when(
      companyRepository.saveOne(
        deepEqual(
          CompanyAggregate.create({
            id: 0,
            name: data.company.name,
            address: data.company.address,
            phone: data.company.phone,
            annualSales: data.company.annualSales,
            companyBusinessType: data.company.companyBusinessType,
            companyEmployeeSize: data.company.companyEmployeeSize,
          }),
        ),
      ),
    ).thenResolve(
      CompanyAggregate.create({
        id: 1000,
        name: data.company.name,
        address: data.company.address,
        phone: data.company.phone,
        annualSales: data.company.annualSales,
        companyBusinessType: data.company.companyBusinessType,
        companyEmployeeSize: data.company.companyEmployeeSize,
      }),
    );
    when(
      userRepository.saveOne(
        deepEqual(
          UserAggregate.create({
            id: 0,
            email: data.email,
            password: '$hashed_password',
            name: data.name,
            phone: data.phone,
            jobTitle: data.jobTitle,
            companyId: 1000,
            userFiles: [],
            userVerfications: [],
            userRoles: ['member'],
            userTermses: data.userTermses,
          }),
        ),
      ),
    ).thenResolve(
      UserAggregate.create({
        id: 2000,
        email: data.email,
        password: '$hashed_password',
        name: data.name,
        phone: data.phone,
        jobTitle: data.jobTitle,
        companyId: 1000,
        userFiles: [],
        userVerfications: [],
        userRoles: ['member'],
        userTermses: data.userTermses,
      }),
    );

    // when
    const result = await signUpUserPort.execute(data);

    // then
    expect(result.id).toEqual(2000);
  });

  it('if userTermses uniq length === 3, fail', async () => {
    // given
    const data: SignUpUserData = {
      email: 'test3@test.com',
      password: 'qwer1234!',
      name: '테스트',
      jobTitle: '대리',
      phone: '010-1234-1236',
      userTermses: [
        'privacy_policy',
        'sensitive_information',
        'unique_identification_information',
        'unique_identification_information',
      ],
      company: {
        name: '테스트회사',
        address: '서울특별시 관악구',
        phone: '010-1234-1234',
        annualSales: 7,
        companyBusinessType: 'primary',
        companyEmployeeSize: 'between_100_200',
      },
    };

    when(hashManager.generateHash(data.password, 12)).thenResolve('$hashed_password');
    when(
      companyRepository.saveOne(
        deepEqual(
          CompanyAggregate.create({
            id: 0,
            name: data.company.name,
            address: data.company.address,
            phone: data.company.phone,
            annualSales: data.company.annualSales,
            companyBusinessType: data.company.companyBusinessType,
            companyEmployeeSize: data.company.companyEmployeeSize,
          }),
        ),
      ),
    ).thenResolve(
      CompanyAggregate.create({
        id: 1000,
        name: data.company.name,
        address: data.company.address,
        phone: data.company.phone,
        annualSales: data.company.annualSales,
        companyBusinessType: data.company.companyBusinessType,
        companyEmployeeSize: data.company.companyEmployeeSize,
      }),
    );
    when(
      userRepository.saveOne(
        deepEqual(
          UserAggregate.create({
            id: 0,
            email: data.email,
            password: '$hashed_password',
            name: data.name,
            phone: data.phone,
            jobTitle: data.jobTitle,
            companyId: 1000,
            userFiles: [],
            userVerfications: [],
            userRoles: ['member'],
            userTermses: data.userTermses,
          }),
        ),
      ),
    ).thenResolve(
      UserAggregate.create({
        id: 2000,
        email: data.email,
        password: '$hashed_password',
        name: data.name,
        phone: data.phone,
        jobTitle: data.jobTitle,
        companyId: 1000,
        userFiles: [],
        userVerfications: [],
        userRoles: ['member'],
        userTermses: data.userTermses,
      }),
    );

    // when
    const result = (await signUpUserPort.execute(data).catch((err: Error) => err)) as Error;

    // then
    expect(result.message).toEqual('terms agreement required.');
  });
});
