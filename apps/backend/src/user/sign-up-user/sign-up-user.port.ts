import { CompanyRepository, HashManager, PrimaryPort, UserRepository } from '@lib/application';
import { ArrayUtil, UserTermsEnum } from '@lib/common';
import { CompanyAggregate, UserAggregate } from '@lib/domain';
import { Transaction } from '@lib/infrastructure';
import { BadRequestException, Injectable } from '@nestjs/common';
import { SignUpUserData, SignUpUserView } from './sign-up-user.type';

@Injectable()
export class SignUpUserPort implements PrimaryPort {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly companyRepository: CompanyRepository,
    private readonly hashManager: HashManager,
  ) {}

  @Transaction()
  async execute(data: SignUpUserData): Promise<SignUpUserView> {
    const { company, password, userTermses, ...etc } = data;

    const newCompanyAgg = await this.companyRepository.saveOne(
      CompanyAggregate.create({
        id: 0,
        ...company,
      }),
    );
    const newUserAgg = await this.userRepository.saveOne(
      UserAggregate.create({
        id: 0,
        ...etc,
        password: await this.hashManager.generateHash(password, 12),
        companyId: newCompanyAgg.id,
        userFiles: [],
        userVerfications: [],
        userRoles: ['member'],
        userTermses: this.validateUserTermses(userTermses),
      }),
    );

    return { id: newUserAgg.id };
  }

  private validateUserTermses(userTermses: UserTermsEnum[]) {
    const uniqTermses = ArrayUtil.toUnique(userTermses);
    if (uniqTermses.length !== 4) {
      throw new BadRequestException('terms agreement required.');
    }

    return uniqTermses;
  }
}
