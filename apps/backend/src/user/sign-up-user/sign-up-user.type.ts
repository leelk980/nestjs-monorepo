import { CompanyBusinessTypeEnum, CompanyEmployeeSizeEnum, UserTermsEnum } from '@lib/common';

export interface SignUpUserData {
  /** @format email */
  email: string;

  /** @pattern ^(?=.*[!@#$%^&*()_\-+=|\\{}[\]:;"'<>,.?/~`])((?=.*\d)(?=.*[a-zA-Z])).{8,20}$ */
  password: string;

  /** @minLength 1 */
  name: string;

  /** @minLength 1 */
  jobTitle: string;

  /** @pattern ^\d{3}-\d{4}-\d{4}$ */
  phone: string;

  /** @minItems 4 */
  /** @maxItems 4 */
  userTermses: UserTermsEnum[];

  company: {
    /** @minLength 1 */
    name: string;

    /** @minLength 1 */
    address: string;

    /** @pattern ^\d{3}-\d{4}-\d{4}$ */
    phone: string;

    /** @minimum 0 */
    annualSales: number;

    companyBusinessType: CompanyBusinessTypeEnum;

    companyEmployeeSize: CompanyEmployeeSizeEnum;
  };
}

export interface SignUpUserView {
  id: number;
}
