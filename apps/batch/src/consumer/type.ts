import { domain_event } from '@prisma/client';

export type DomainEvent = domain_event;

export type DomainEventHandlerMap<T extends string> = {
  [key in T]: (event: DomainEvent) => Promise<void>;
};
