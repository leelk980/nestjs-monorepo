import { Injectable, NotImplementedException, OnModuleInit } from '@nestjs/common';
import { DiscoveryService } from '@nestjs/core';
import { Cron, CronExpression } from '@nestjs/schedule';
import { PrismaClient } from '@prisma/client';
import { DomainEventHandlerMap } from './type';

@Injectable()
export class DomainEventConsumer implements OnModuleInit {
  handlerMap!: DomainEventHandlerMap<string>;

  @Cron(CronExpression.EVERY_SECOND)
  async handleCron() {
    const events = await this.prismaClient.$transaction(
      async (trx) => {
        const events = await trx.domain_event.findMany({
          where: { status: 'ready' },
          take: 10,
        });

        if (events.length !== 0) {
          await trx.domain_event.updateMany({
            where: { id: { in: events.map((each) => each.id) } },
            data: { status: 'progress' },
          });
        }

        return events;
      },
      { isolationLevel: 'Serializable' },
    );
    if (events.length === 0) return;

    const results = await Promise.allSettled(
      events.map(async (event) => {
        const handler = this.handlerMap[event.name];
        if (!handler)
          throw new NotImplementedException(
            `domain event handler is not implemented. ${event.name}.`,
          );

        return handler(event);
      }),
    );

    const { doneIds, failEvents } = events.reduce(
      (
        acc: {
          doneIds: number[];
          failEvents: { id: number; reason: string }[];
        },
        each,
        index,
      ) => {
        const result = results[index];
        if (!result) {
          acc.failEvents.push({ id: each.id, reason: 'result not found.' });
        } else if (result.status === 'rejected') {
          acc.failEvents.push({
            id: each.id,
            reason: (() => {
              if (result.reason instanceof Error) {
                return result.reason.stack || result.reason.message;
              }

              return JSON.stringify(result.reason);
            })(),
          });
        } else if (result.status === 'fulfilled') {
          acc.doneIds.push(each.id);
        } else {
          acc.failEvents.push({ id: each.id, reason: 'invalid case.' });
        }

        return acc;
      },
      {
        doneIds: [],
        failEvents: [],
      },
    );

    if (doneIds.length !== 0) {
      await this.prismaClient.domain_event.updateMany({
        where: { id: { in: doneIds } },
        data: { status: 'done', processedAt: new Date() },
      });
    }

    if (failEvents.length !== 0) {
      await Promise.all(
        failEvents.map(async (event) =>
          this.prismaClient.domain_event.update({
            where: { id: event.id },
            data: { status: 'fail', errorLog: event.reason },
          }),
        ),
      );
    }
  }

  constructor(
    private readonly discoveryService: DiscoveryService,
    private readonly prismaClient: PrismaClient,
  ) {}

  onModuleInit() {
    this.handlerMap = this.discoveryService
      .getProviders()
      .filter(({ name }) => typeof name === 'string' && name.endsWith('DomainEventHandlerMap'))
      .map(({ instance }) => instance)
      .reduce((acc, each) => ({ ...acc, ...each }), {});
  }
}
