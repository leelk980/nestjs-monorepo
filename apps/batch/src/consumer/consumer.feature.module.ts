import { Module } from '@nestjs/common';
import { DiscoveryModule } from '@nestjs/core';
import { PrismaClient } from '@prisma/client';
import { DomainEventConsumer } from './domain-event.consumer';
import { ScheduledTaskConsumer } from './scheduled-task.consumer';

@Module({
  imports: [DiscoveryModule],
  providers: [
    DomainEventConsumer,
    ScheduledTaskConsumer,
    {
      useValue: new PrismaClient(),
      provide: PrismaClient,
    },
  ],
})
export class ConsumerFeatureModule {}
