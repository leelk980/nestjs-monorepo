import { environment } from '@lib/common';
import { ManagerInfraModule, RepositoryInfraModule } from '@lib/infrastructure';
import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { ConsumerFeatureModule } from './consumer/consumer.feature.module';
import { HandlerFeatureModule } from './handler/handler.feature.module';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ManagerInfraModule.forRoot({
      asyncStorage: true,
    }),
    RepositoryInfraModule.forRoot({
      type: 'mysql',
      host: environment.database.host,
      port: environment.database.port,
      username: environment.database.username,
      password: environment.database.password,
      name: environment.database.name,
    }),
    ConsumerFeatureModule,
    HandlerFeatureModule,
  ],
})
export class AppModule {}
