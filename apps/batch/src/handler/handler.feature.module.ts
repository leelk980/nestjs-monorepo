import { CarbonEmissionRepository, CompanyRepository, UserRepository } from '@lib/application';
import { RepositoryInfraModule } from '@lib/infrastructure';
import { Module } from '@nestjs/common';
import {
  CarbonEmissionDomainEventHandlerMap,
  CompanyDomainEventHandlerMap,
  UserDomainEventHandlerMap,
} from './domain-event';

@Module({
  imports: [
    RepositoryInfraModule.forFeature(UserRepository, CompanyRepository, CarbonEmissionRepository),
  ],
  providers: [
    UserDomainEventHandlerMap,
    CompanyDomainEventHandlerMap,
    CarbonEmissionDomainEventHandlerMap,
  ],
})
export class HandlerFeatureModule {}
