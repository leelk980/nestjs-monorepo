import { UserRepository } from '@lib/application';
import { UserDomainEvent } from '@lib/domain';
import { Injectable } from '@nestjs/common';
import { DomainEvent, DomainEventHandlerMap } from '../../consumer/type';

@Injectable()
export class UserDomainEventHandlerMap implements DomainEventHandlerMap<UserDomainEvent> {
  constructor(private readonly userRepository: UserRepository) {}

  user_created = async (event: DomainEvent) => {
    const userAgg = await this.userRepository.findOneById(event.pk);
    console.log(`${userAgg.name}'s ${event.name} event handled`);
  };

  user_updated = async (event: DomainEvent) => {
    const userAgg = await this.userRepository.findOneById(event.pk);
    console.log(`${userAgg.name}'s ${event.name} event handled`);
  };

  user_verification_upserted = async (event: DomainEvent) => {
    const userAgg = await this.userRepository.findOneById(event.pk);
    console.log(`${userAgg.name}'s ${event.name} event handled`);
  };

  user_verification_approved = async (event: DomainEvent) => {
    const userAgg = await this.userRepository.findOneById(event.pk);
    console.log(`${userAgg.name}'s ${event.name} event handled`);
  };
}
