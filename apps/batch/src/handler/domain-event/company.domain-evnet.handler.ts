import { CompanyRepository } from '@lib/application';
import { CompanyDomainEvent } from '@lib/domain';
import { Injectable } from '@nestjs/common';
import { DomainEvent, DomainEventHandlerMap } from '../../consumer/type';

@Injectable()
export class CompanyDomainEventHandlerMap implements DomainEventHandlerMap<CompanyDomainEvent> {
  constructor(private readonly companyRepository: CompanyRepository) {}

  company_created = async (event: DomainEvent) => {
    const companyAgg = await this.companyRepository.findOneById(event.pk);
    console.log(`${companyAgg.name}'s ${event.name} event handled`);
  };

  company_updated = async (event: DomainEvent) => {
    const companyAgg = await this.companyRepository.findOneById(event.pk);
    console.log(`${companyAgg.name}'s ${event.name} event handled`);
  };
}
