import { CarbonEmissionRepository } from '@lib/application';
import { CarbonEmissionDomainEvent } from '@lib/domain';
import { Injectable } from '@nestjs/common';
import { DomainEvent, DomainEventHandlerMap } from '../../consumer/type';

@Injectable()
export class CarbonEmissionDomainEventHandlerMap
  implements DomainEventHandlerMap<CarbonEmissionDomainEvent>
{
  constructor(private readonly carbonEmissionRepository: CarbonEmissionRepository) {}

  carbon_emission_created = async (event: DomainEvent) => {
    const carbonEmissionAgg = await this.carbonEmissionRepository.findOneById(event.pk);
    console.log(`${carbonEmissionAgg.id}'s ${event.name} event handled`);
  };

  carbon_emission_updated = async (event: DomainEvent) => {
    const carbonEmissionAgg = await this.carbonEmissionRepository.findOneById(event.pk);
    console.log(`${carbonEmissionAgg.id}'s ${event.name} event handled`);
  };
}
