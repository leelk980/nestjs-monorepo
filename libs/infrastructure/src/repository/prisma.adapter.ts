import { Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { Prisma, PrismaClient } from '@prisma/client';
import { ClsAdapter } from '../manager/cls.adapter';

@Injectable()
export class PrismaAdapter implements OnModuleInit, OnModuleDestroy {
  public readonly client = new PrismaClient({
    log: [
      {
        emit: 'stdout',
        level: 'query',
      },
      {
        emit: 'stdout',
        level: 'error',
      },
      {
        emit: 'stdout',
        level: 'info',
      },
      {
        emit: 'stdout',
        level: 'warn',
      },
    ],
  });

  constructor(private readonly clsAdapter: ClsAdapter) {}

  async onModuleInit() {
    await this.client.$connect();

    this.client.$use(async (params, next) => {
      if (params.action.startsWith('find')) {
        params.args.where.deletedAt = params.args.where.deletedAt || null;
      }

      return next(params);
    });
  }

  async onModuleDestroy() {
    await this.client.$disconnect();
  }

  queryRunner<T extends EntityName<keyof Prisma.TransactionClient>>(
    entity: T,
  ): Prisma.TransactionClient[T] {
    const transaction = this.clsAdapter.getItem('transaction') as Prisma.TransactionClient;
    if (transaction) {
      return transaction[entity];
    }

    return this.client[entity];
  }
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
type EntityName<T> = T extends `$${infer _R}` ? never : T;
