import { AbstractClassType, ClassType } from '@lib/common';
import { PrismaCarbonEmissionRepository } from './impl/prisma.carbon-emission.repository';
import { PrismaCompanyRepository } from './impl/prisma.company.repository';
import { PrismaUserRepository } from './impl/prisma.user.repository';
import { PrismaAdapter } from './prisma.adapter';
import {
  DynamicModule,
  forwardRef,
  ForwardReference,
  Module,
  NotImplementedException,
} from '@nestjs/common';

@Module({})
export class RepositoryInfraModule {
  static repositories = [
    PrismaUserRepository,
    PrismaCompanyRepository,
    PrismaCarbonEmissionRepository,
  ];
  static repositoryMap: Record<string, ClassType> = RepositoryInfraModule.repositories.reduce(
    (acc: Record<string, any>, each) => {
      const ormName = each.name.startsWith('Prisma') ? 'Prisma' : 'Typeorm';
      const repositoryName = each.name.split(ormName)[1];
      if (!repositoryName) {
        throw new NotImplementedException(`invalid reopsitory name. ${each}`);
      }

      acc[repositoryName] = each;

      return acc;
    },
    {},
  );

  static forRoot(options: {
    type: 'mysql' | 'postgres';
    host: string;
    port: number;
    username: string;
    password: string;
    name: string;
    schema?: string;
  }): DynamicModule {
    const { type, host, port, username, password, name, schema } = options;

    // for prisma
    process.env[
      'DATABASE_URL'
    ] = `${type}://${username}:${password}@${host}:${port}/${name}?schema=${schema}`;

    const providers = [...this.repositories, PrismaAdapter];

    return {
      module: RepositoryInfraModule,
      providers,
      exports: providers,
      global: true,
    };
  }

  static forFeature(...repositories: AbstractClassType[]): ForwardReference {
    return forwardRef(() => this.forFeatureImpl(...repositories));
  }

  private static forFeatureImpl(...repositories: AbstractClassType[]): DynamicModule {
    const providers = repositories.map((each) => ({
      provide: each,
      useExisting: this.repositoryMap[each.name],
    }));

    return {
      module: RepositoryInfraModule,
      providers: providers,
      exports: providers,
      global: false,
    };
  }
}
