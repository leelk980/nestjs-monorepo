import { entries, fromEntries, groupBy, indexBy, map, pipe } from '@fxts/core';
import { BadRequestException } from '@nestjs/common';
import { config, config_type } from '@prisma/client';

export class ConfigUtil {
  static createConfigMap(configs: config[]) {
    return pipe(
      groupBy((each) => each.type, configs),
      entries,
      map((entry): [typeof key, Record<string, config>] => {
        const [key, value] = entry;
        const valueMap = indexBy((each) => each.name, value as config[]);

        return [key, valueMap];
      }),
      fromEntries,
      this.handle,
    );
  }

  private static handle(configMap: Record<config_type, Record<string, config>>) {
    return {
      getId(type: config_type, name: string): number {
        const configId = configMap[type][name]?.id;
        if (!configId) {
          throw new BadRequestException(`invalid ${type} config. ${name}`);
        }

        return configId;
      },
    };
  }
}
