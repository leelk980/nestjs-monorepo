import { CarbonEmissionAggregate } from '@lib/domain';
import { Injectable } from '@nestjs/common';
import { CarbonEmissionRepository } from 'libs/application/src/secondary/repository/carbon-emission.repository';
import { ConfigUtil } from './config.util';
import { PrismaAdapter } from '../prisma.adapter';
import {
  CarbonEmissionFaciltityEnum,
  CarbonEmissionHeatingEnum,
  CarbonEmissionVehicleEnum,
  ObjectUtil,
  OmitProperties,
  Pagination,
} from '@lib/common';
import {
  carbon_emission,
  carbon_emission_facility,
  carbon_emission_heating,
  carbon_emission_vehicle,
  config,
  Prisma,
} from '@prisma/client';

type CarbonEmissionEntity = carbon_emission & {
  carbon_emission_heating: (carbon_emission_heating & {
    config: config;
  })[];
  carbon_emission_facility: (carbon_emission_facility & {
    config: config;
  })[];
  carbon_emission_vehicle: (carbon_emission_vehicle & {
    config: config;
  })[];
};

const includeAll = {
  carbon_emission_heating: { include: { config: true } },
  carbon_emission_facility: { include: { config: true } },
  carbon_emission_vehicle: { include: { config: true } },
} satisfies OmitProperties<Required<Prisma.carbon_emissionInclude>, '_count'>;

@Injectable()
export class PrismaCarbonEmissionRepository implements CarbonEmissionRepository {
  constructor(private readonly prismaAdapter: PrismaAdapter) {}

  async saveOne(agg: CarbonEmissionAggregate): Promise<CarbonEmissionAggregate> {
    const {
      id,
      userId,
      carbonEmissionHeatings,
      carbonEmissionFacilities,
      carbonEmissionVehicles,
      domainEvents,
      ...etc
    } = ObjectUtil.prune(ObjectUtil.clone(agg));

    const configMap = ConfigUtil.createConfigMap(
      await this.prismaAdapter.queryRunner('config').findMany({
        where: {
          OR: [
            { type: 'carbon_emission_heating' },
            { type: 'carbon_emission_faciltity' },
            { type: 'carbon_emission_vehicle' },
          ],
        },
      }),
    );

    const carbonEmissionEntity = await (async () => {
      if (id === 0) {
        return this.prismaAdapter.queryRunner('carbon_emission').create({
          data: {
            ...etc,
            user_id: userId,
            carbon_emission_heating: {
              createMany: {
                data: carbonEmissionHeatings.map(({ type, usage }) => ({
                  config_id: configMap.getId('carbon_emission_heating', type),
                  usage,
                })),
              },
            },
            carbon_emission_facility: {
              createMany: {
                data: carbonEmissionFacilities.map(({ type, count, usage }) => ({
                  config_id: configMap.getId('carbon_emission_faciltity', type),
                  count,
                  usage,
                })),
              },
            },
            carbon_emission_vehicle: {
              createMany: {
                data: carbonEmissionVehicles.map(({ type, count, usage }) => ({
                  config_id: configMap.getId('carbon_emission_vehicle', type),
                  count,
                  usage,
                })),
              },
            },
            createdAt: new Date(),
            updatedAt: new Date(),
          },
          include: includeAll,
        });
      }

      return this.prismaAdapter.queryRunner('carbon_emission').update({
        where: { id },
        data: {
          ...etc,
          user_id: userId,
          carbon_emission_heating: {
            deleteMany: { carbon_emission_id: id },
            createMany: {
              data: carbonEmissionHeatings.map(({ type, usage }) => ({
                config_id: configMap.getId('carbon_emission_heating', type),
                usage,
              })),
            },
          },
          carbon_emission_facility: {
            deleteMany: { carbon_emission_id: id },
            createMany: {
              data: carbonEmissionFacilities.map(({ type, count, usage }) => ({
                config_id: configMap.getId('carbon_emission_faciltity', type),
                count,
                usage,
              })),
            },
          },
          carbon_emission_vehicle: {
            deleteMany: { carbon_emission_id: id },
            createMany: {
              data: carbonEmissionVehicles.map(({ type, count, usage }) => ({
                config_id: configMap.getId('carbon_emission_vehicle', type),
                count,
                usage,
              })),
            },
          },
          updatedAt: new Date(),
        },
        include: includeAll,
      });
    })();

    await this.publishDomainEvents(carbonEmissionEntity.id, domainEvents);

    return this.convertToAgg(carbonEmissionEntity);
  }

  async softDeleteOne(agg: CarbonEmissionAggregate): Promise<CarbonEmissionAggregate> {
    const entity = await this.prismaAdapter.queryRunner('carbon_emission').update({
      where: { id: agg.id },
      data: { deletedAt: new Date() },
      include: includeAll,
    });

    return this.convertToAgg(entity);
  }

  async findMany(params: {
    filter: { userId: number };
    page: { size: number; index: number };
  }): Promise<Pagination<CarbonEmissionAggregate>> {
    const { filter, page } = params;

    const ids = (
      await this.prismaAdapter.queryRunner('carbon_emission').findMany({
        where: { user_id: filter.userId },
        select: { id: true },
      })
    ).map((each) => each.id);

    const entities = await this.prismaAdapter.queryRunner('carbon_emission').findMany({
      where: { id: { in: ids } },
      skip: page.size * (page.index - 1),
      take: page.size,
      include: includeAll,
    });

    return {
      data: entities.map(this.convertToAgg),
      page: {
        size: page.size,
        index: page.index,
        lastIndex: Math.ceil(ids.length / page.size),
        totalElements: ids.length,
      },
    };
  }

  async findOneById(id: number): Promise<CarbonEmissionAggregate> {
    const carbonEmissionEntity = await this.prismaAdapter
      .queryRunner('carbon_emission')
      .findFirstOrThrow({ where: { id }, include: includeAll });

    return this.convertToAgg(carbonEmissionEntity);
  }

  private convertToAgg(entity: CarbonEmissionEntity): CarbonEmissionAggregate {
    return CarbonEmissionAggregate.create({
      ...entity,
      caluatedValue: entity.caluatedValue.toNumber(),
      userId: entity.user_id,
      carbonEmissionHeatings: entity.carbon_emission_heating.map((each) => ({
        type: each.config.name as CarbonEmissionHeatingEnum,
        usage: each.usage,
      })),
      carbonEmissionFacilities: entity.carbon_emission_facility.map((each) => ({
        type: each.config.name as CarbonEmissionFaciltityEnum,
        count: each.count,
        usage: each.usage,
      })),
      carbonEmissionVehicles: entity.carbon_emission_vehicle.map((each) => ({
        type: each.config.name as CarbonEmissionVehicleEnum,
        count: each.count,
        usage: each.usage,
      })),
    });
  }

  private async publishDomainEvents(pk: number, events: string[]) {
    return this.prismaAdapter.queryRunner('domain_event').createMany({
      data: events.map((event) => ({
        pk,
        aggregate: 'carbon_emission',
        name: event,
        status: 'ready',
        errorLog: null,
        processedAt: null,
        createdAt: new Date(),
      })),
    });
  }
}
