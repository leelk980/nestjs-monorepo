import { UserRepository } from '@lib/application';
import { UserAggregate } from '@lib/domain';
import { Injectable } from '@nestjs/common';
import {
  config,
  Prisma,
  user,
  user_file,
  user_role,
  user_terms,
  user_verification,
} from '@prisma/client';
import { ConfigUtil } from './config.util';
import { PrismaAdapter } from '../prisma.adapter';
import {
  ObjectUtil,
  OmitProperties,
  UserFileEnum,
  UserRoleEnum,
  UserTermsEnum,
  UserVerificationEnum,
} from '@lib/common';

type UserEntity = user & {
  user_file: (user_file & {
    config: config;
  })[];
  user_verification: (user_verification & {
    config: config;
  })[];
  user_role: (user_role & {
    config: config;
  })[];
  user_terms: (user_terms & {
    config: config;
  })[];
};

const includeAll = {
  user_file: { include: { config: true } },
  user_verification: { include: { config: true } },
  user_role: { include: { config: true } },
  user_terms: { include: { config: true } },
} satisfies OmitProperties<Required<Prisma.userInclude>, '_count'>;

@Injectable()
export class PrismaUserRepository implements UserRepository {
  constructor(private readonly prismaAdapter: PrismaAdapter) {}

  async saveOne(agg: UserAggregate): Promise<UserAggregate> {
    const {
      id,
      companyId,
      userFiles,
      userVerfications,
      userRoles,
      userTermses,
      domainEvents,
      ...etc
    } = ObjectUtil.prune(ObjectUtil.clone(agg));

    const configMap = ConfigUtil.createConfigMap(
      await this.prismaAdapter.queryRunner('config').findMany({
        where: {
          OR: [
            { type: 'user_file' },
            { type: 'user_verification' },
            { type: 'user_role' },
            { type: 'user_terms' },
          ],
        },
      }),
    );

    const userEntity = await (async () => {
      if (id === 0) {
        return this.prismaAdapter.queryRunner('user').create({
          data: {
            ...etc,
            company_id: companyId,
            user_file: {
              createMany: {
                data: userFiles.map((each) => ({
                  config_id: configMap.getId('user_file', each.type),
                  storagePath: each.storagePath,
                })),
              },
            },
            user_verification: {
              createMany: {
                data: userVerfications.map((each) => ({
                  config_id: configMap.getId('user_verification', each.type),
                  verificationCode: each.verificationCode,
                  isVerified: each.isVerified,
                  sentAt: each.sentAt,
                  value: each.value,
                })),
              },
            },
            user_role: {
              createMany: {
                data: userRoles.map((each) => ({
                  config_id: configMap.getId('user_role', each),
                })),
              },
            },
            user_terms: {
              createMany: {
                data: userTermses.map((each) => ({
                  config_id: configMap.getId('user_terms', each),
                })),
              },
            },
            createdAt: new Date(),
            updatedAt: new Date(),
          },
          include: includeAll,
        });
      }

      return this.prismaAdapter.queryRunner('user').update({
        where: { id },
        data: {
          ...etc,
          company_id: companyId,
          user_file: {
            deleteMany: { user_id: id },
            createMany: {
              data: userFiles.map((each) => ({
                config_id: configMap.getId('user_file', each.type),
                storagePath: each.storagePath,
              })),
            },
          },
          user_verification: {
            deleteMany: { user_id: id },
            createMany: {
              data: userVerfications.map((each) => ({
                config_id: configMap.getId('user_verification', each.type),
                verificationCode: each.verificationCode,
                isVerified: each.isVerified,
                sentAt: each.sentAt,
                value: each.value,
              })),
            },
          },
          user_role: {
            deleteMany: { user_id: id },
            createMany: {
              data: userRoles.map((each) => ({
                config_id: configMap.getId('user_role', each),
              })),
            },
          },
          user_terms: {
            deleteMany: { user_id: id },
            createMany: {
              data: userTermses.map((each) => ({
                config_id: configMap.getId('user_terms', each),
              })),
            },
          },
          updatedAt: new Date(),
        },
        include: includeAll,
      });
    })();

    await this.publishDomainEvents(userEntity.id, domainEvents);

    return this.convertToAgg(userEntity);
  }

  async softDeleteOne(agg: UserAggregate): Promise<UserAggregate> {
    const entity = await this.prismaAdapter.queryRunner('user').update({
      where: { id: agg.id },
      data: { deletedAt: new Date() },
      include: includeAll,
    });

    return this.convertToAgg(entity);
  }

  async findOneById(id: number): Promise<UserAggregate> {
    const userEntity = await this.prismaAdapter
      .queryRunner('user')
      .findFirstOrThrow({ where: { id }, include: includeAll });

    return this.convertToAgg(userEntity);
  }

  async findOneByEmailOrNull(email: string): Promise<UserAggregate | null> {
    const userEntity = await this.prismaAdapter
      .queryRunner('user')
      .findFirst({ where: { email }, include: includeAll });

    return userEntity ? this.convertToAgg(userEntity) : null;
  }

  async findOneByPhoneOrNull(phone: string): Promise<UserAggregate | null> {
    const userEntity = await this.prismaAdapter
      .queryRunner('user')
      .findFirst({ where: { phone }, include: includeAll });

    return userEntity ? this.convertToAgg(userEntity) : null;
  }

  private convertToAgg(entity: UserEntity): UserAggregate {
    return UserAggregate.create({
      ...entity,
      companyId: entity.company_id,
      userFiles: entity.user_file.map((each) => ({
        type: each.config.name as UserFileEnum,
        storagePath: each.storagePath,
      })),
      userVerfications: entity.user_verification.map((each) => ({
        type: each.config.name as UserVerificationEnum,
        verificationCode: each.verificationCode,
        isVerified: each.isVerified,
        sentAt: each.sentAt,
        value: each.value,
      })),
      userRoles: entity.user_role.map((each) => each.config.name as UserRoleEnum),
      userTermses: entity.user_terms.map((each) => each.config.name as UserTermsEnum),
    });
  }

  private async publishDomainEvents(pk: number, events: string[]) {
    return this.prismaAdapter.queryRunner('domain_event').createMany({
      data: events.map((event) => ({
        pk,
        aggregate: 'user',
        name: event,
        status: 'ready',
        errorLog: null,
        processedAt: null,
        createdAt: new Date(),
      })),
    });
  }
}
