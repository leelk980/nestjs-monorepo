import { CompanyRepository } from '@lib/application';
import { CompanyAggregate } from '@lib/domain';
import { Injectable } from '@nestjs/common';
import { ConfigUtil } from './config.util';
import { PrismaAdapter } from '../prisma.adapter';
import {
  CompanyBusinessTypeEnum,
  CompanyEmployeeSizeEnum,
  ObjectUtil,
  OmitProperties,
} from '@lib/common';
import {
  company,
  company_business_type,
  company_employee_size,
  config,
  Prisma,
} from '@prisma/client';

type CompanyEntity = company & {
  company_business_type: (company_business_type & {
    config: config;
  })[];
  company_employee_size: (company_employee_size & {
    config: config;
  })[];
};

const includeAll = {
  company_business_type: { include: { config: true } },
  company_employee_size: { include: { config: true } },
} satisfies OmitProperties<Required<Prisma.companyInclude>, '_count'>;

@Injectable()
export class PrismaCompanyRepository implements CompanyRepository {
  constructor(private readonly prismaAdapter: PrismaAdapter) {}

  async saveOne(agg: CompanyAggregate): Promise<CompanyAggregate> {
    const { id, companyBusinessType, companyEmployeeSize, domainEvents, ...etc } = ObjectUtil.prune(
      ObjectUtil.clone(agg),
    );

    const configMap = ConfigUtil.createConfigMap(
      await this.prismaAdapter.queryRunner('config').findMany({
        where: { OR: [{ type: 'company_business_type' }, { type: 'company_employee_size' }] },
      }),
    );

    const companyEntity = await (async () => {
      if (id === 0) {
        return this.prismaAdapter.queryRunner('company').create({
          data: {
            ...etc,
            company_business_type: {
              create: { config_id: configMap.getId('company_business_type', companyBusinessType) },
            },
            company_employee_size: {
              create: { config_id: configMap.getId('company_employee_size', companyEmployeeSize) },
            },
            createdAt: new Date(),
            updatedAt: new Date(),
          },
          include: includeAll,
        });
      }

      return this.prismaAdapter.queryRunner('company').update({
        where: { id },
        data: {
          ...etc,
          company_business_type: {
            deleteMany: { company_id: id },
            create: { config_id: configMap.getId('company_business_type', companyBusinessType) },
          },
          company_employee_size: {
            deleteMany: { company_id: id },
            create: { config_id: configMap.getId('company_employee_size', companyEmployeeSize) },
          },
          updatedAt: new Date(),
        },
        include: includeAll,
      });
    })();

    await this.publishDomainEvents(companyEntity.id, domainEvents);

    return this.convertToAgg(companyEntity);
  }

  async softDeleteOne(agg: CompanyAggregate): Promise<CompanyAggregate> {
    const entity = await this.prismaAdapter.queryRunner('company').update({
      where: { id: agg.id },
      data: { deletedAt: new Date() },
      include: includeAll,
    });

    return this.convertToAgg(entity);
  }

  async findOneById(id: number): Promise<CompanyAggregate> {
    const entity = await this.prismaAdapter
      .queryRunner('company')
      .findFirstOrThrow({ where: { id }, include: includeAll });

    return this.convertToAgg(entity);
  }

  private convertToAgg(entity: CompanyEntity): CompanyAggregate {
    return CompanyAggregate.create({
      ...entity,
      companyBusinessType: entity.company_business_type[0]?.config.name as CompanyBusinessTypeEnum,
      companyEmployeeSize: entity.company_employee_size[0]?.config.name as CompanyEmployeeSizeEnum,
    });
  }

  private async publishDomainEvents(pk: number, events: string[]) {
    return this.prismaAdapter.queryRunner('domain_event').createMany({
      data: events.map((event) => ({
        pk,
        aggregate: 'company',
        name: event,
        status: 'ready',
        errorLog: null,
        processedAt: null,
        createdAt: new Date(),
      })),
    });
  }
}
