import { Aspect, LazyDecorator, WrapParams } from '@toss/nestjs-aop';
import { createDecorator } from '@toss/nestjs-aop';
import { ClsAdapter } from '../manager/cls.adapter';
import { PrismaAdapter } from '../repository/prisma.adapter';

const TransactionSymbol = Symbol('Transaction');

@Aspect(TransactionSymbol)
export class TransactionAspect implements LazyDecorator {
  constructor(
    private readonly prismaAdapter: PrismaAdapter,
    private readonly clsAdapter: ClsAdapter,
  ) {}

  wrap(params: WrapParams) {
    const { method } = params;

    return (...params: any[]) =>
      this.prismaAdapter.client.$transaction(async (trx) => {
        this.clsAdapter.setItem('transaction', trx);
        return method(...params);
      });
  }
}

export const Transaction = () => createDecorator(TransactionSymbol, true);
