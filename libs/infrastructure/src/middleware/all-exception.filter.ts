import { environment } from '@lib/common';
import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { Response } from 'express';

@Catch()
export class AllExceptionFilter implements ExceptionFilter {
  constructor() {
    //
  }

  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const res = ctx.getResponse<Response>();
    // const req = ctx.getRequest<Request>();

    res.json({
      status: exception.status || 500,
      name: exception.name,
      message: exception.message || 'internal server error',
      stack: environment.node.env !== 'production' && exception?.stack,
      response: environment.node.env !== 'production' && exception?.response,
    });
  }
}
