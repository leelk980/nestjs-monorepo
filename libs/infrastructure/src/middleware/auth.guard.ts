import { ArrayUtil, UserRoleEnum } from '@lib/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { ClsAdapter } from '../manager/cls.adapter';
import { JwtAdapter } from '../manager/jwt.adpater';
import {
  CanActivate,
  ExecutionContext,
  Injectable,
  NotImplementedException,
  SetMetadata,
} from '@nestjs/common';

const AuthSymbol = Symbol('auth');

type AuthType = UserRoleEnum | 'public';

export const Auth = (...auths: AuthType[]) => SetMetadata(AuthSymbol, auths);

@Injectable()
export class AuthGuard implements CanActivate {
  public constructor(
    private readonly reflector: Reflector,
    private readonly jwtAdapater: JwtAdapter,
    private readonly clsAdapter: ClsAdapter,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest<Request>();
    const auths = this.reflector.getAllAndOverride<AuthType[]>(AuthSymbol, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (!auths || auths.length === 0) {
      throw new NotImplementedException(`not specified auth.`);
    }

    if (auths.includes('public')) {
      return true;
    }

    const accessToken = req?.headers?.authorization?.split('Bearer ')[1];
    if (!accessToken) {
      return false;
    }

    const payload = this.jwtAdapater.verify({ type: 'access', token: accessToken });
    // if(payload.userVerficiations.length === 0) {
    // return false;
    // }

    const userRoleMap = ArrayUtil.assoicateBy((key) => key, payload.userRoles);
    if (auths.includes('master') && !userRoleMap.master) {
      return false;
    }

    if (auths.includes('admin') && !userRoleMap.admin && !userRoleMap.master) {
      return false;
    }

    if (
      auths.includes('member') &&
      !userRoleMap.member &&
      !userRoleMap.admin &&
      !userRoleMap.master
    ) {
      return false;
    }

    this.clsAdapter.setItem('currUser', {
      id: payload.userId,
      userVerficiations: payload.userVerficiations,
      userRoles: payload.userRoles,
    });

    return true;
  }
}
