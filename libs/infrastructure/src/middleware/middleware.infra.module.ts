import { DynamicModule, Module, ModuleMetadata, Provider } from '@nestjs/common';
import { APP_FILTER, APP_GUARD } from '@nestjs/core';
import { AopModule } from '@toss/nestjs-aop';
import { AllExceptionFilter } from './all-exception.filter';
import { AuthGuard } from './auth.guard';
import { TransactionAspect } from './transaction.aspect';

@Module({})
export class MiddlewareInfraModule {
  static forRoot(params: {
    guard?: boolean;
    interceptor?: boolean;
    filter?: boolean;
  }): DynamicModule {
    const imports: ModuleMetadata['imports'] = [];
    const providers: Provider[] = [];
    const { guard, interceptor, filter } = params;

    if (guard) {
      providers.push({
        provide: APP_GUARD,
        useClass: AuthGuard,
      });
    }

    if (interceptor) {
      imports.push(AopModule);
      providers.push(TransactionAspect);
    }

    if (filter) {
      providers.push({
        provide: APP_FILTER,
        useClass: AllExceptionFilter,
      });
    }

    return {
      module: MiddlewareInfraModule,
      imports,
      providers,
    };
  }
}
