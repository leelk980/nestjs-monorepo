import { HashManager } from '@lib/application';
import { Injectable } from '@nestjs/common';
import { compare, genSalt, hash } from 'bcryptjs';

@Injectable()
export class BcryptAdapter implements HashManager {
  async generateHash(text: string, rounds = 12): Promise<string> {
    const salt = await new Promise<string>((resolve, reject) => {
      genSalt(rounds, function (err, salt) {
        if (err) reject(err);
        resolve(salt);
      });
    });

    return new Promise<string>((resolve, reject) => {
      hash(text, salt, function (err, hashed) {
        if (err) reject(err);
        resolve(hashed);
      });
    });
  }

  async validateHash(text: string, hash: string): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      compare(text, hash, function (err, result) {
        if (err) reject(err);
        resolve(result);
      });
    });
  }
}
