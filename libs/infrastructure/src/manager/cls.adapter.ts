import { AsyncStorageItem, AsyncStorageManager } from '@lib/application';
import { Injectable } from '@nestjs/common';
import { ClsService } from 'nestjs-cls';

/* @todo 아니 이거 왜 ts-ignore 해야 되는거야 */
/* eslint-disable @typescript-eslint/ban-ts-comment */
@Injectable()
export class ClsAdapter implements AsyncStorageManager {
  constructor(private readonly clsService: ClsService) {}

  // @ts-ignore
  getItem<T extends AsyncStorageItem['key']>(
    key: T,
  ): Extract<AsyncStorageItem, { key: T }>['value'] {
    // @ts-ignore
    return this.clsService.get(key);
  }

  // @ts-ignore
  setItem<T extends AsyncStorageItem['key']>(
    key: T,
    value: Extract<AsyncStorageItem, { key: T }>['value'],
  ): void {
    return this.clsService.set(key, value);
  }
}
