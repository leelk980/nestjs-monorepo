import { AsyncStorageManager, FileStorageManager, HashManager, JwtManager } from '@lib/application';
import { ClsModule } from 'nestjs-cls';
import { AwsS3Adapater } from './aws-s3.adapter';
import { BcryptAdapter } from './bcrypt.adapter';
import { ClsAdapter } from './cls.adapter';
import { JwtAdapter } from './jwt.adpater';
import {
  DynamicModule,
  forwardRef,
  ForwardReference,
  Module,
  NotImplementedException,
} from '@nestjs/common';

type Manager = AsyncStorageManager | FileStorageManager | HashManager | JwtManager;

@Module({})
export class ManagerInfraModule {
  static forRoot(params: {
    asyncStorage?: boolean;
    fileStorage?: boolean;
    hash?: boolean;
    jwt?: boolean;
  }): DynamicModule {
    const { asyncStorage, fileStorage, hash, jwt } = params;
    const imports = [];
    const providers = [];

    if (asyncStorage) {
      imports.push(ClsModule.forRoot({ middleware: { mount: true } }));
      providers.push(ClsAdapter);
    }

    if (fileStorage) {
      providers.push(AwsS3Adapater);
    }

    if (hash) {
      providers.push(BcryptAdapter);
    }

    if (jwt) {
      providers.push(JwtAdapter);
    }

    return {
      module: ManagerInfraModule,
      imports,
      providers,
      exports: providers,
      global: true,
    };
  }

  static forFeature(...params: (abstract new () => Manager)[]): ForwardReference {
    return forwardRef(() => this.forFeatureImpl(params));
  }

  private static forFeatureImpl(params: (abstract new () => Manager)[]): DynamicModule {
    const providers = params.map((each) => {
      return {
        provide: each,
        useExisting: (() => {
          if (each === AsyncStorageManager) {
            return ClsAdapter;
          }

          if (each === FileStorageManager) {
            return AwsS3Adapater;
          }

          if (each === HashManager) {
            return BcryptAdapter;
          }

          if (each === JwtManager) {
            return JwtAdapter;
          }

          throw new NotImplementedException(`invalid manager. ${each}`);
        })(),
      };
    });

    return {
      module: ManagerInfraModule,
      providers: providers,
      exports: providers,
      global: false,
    };
  }
}
