import { FileStorageManager } from '@lib/application';
import { environment } from '@lib/common';
import { Injectable } from '@nestjs/common';
import * as Aws from 'aws-sdk';

Aws.config.update({
  region: 'ap-northeast-2',
  credentials: {
    accessKeyId: environment.aws.access,
    secretAccessKey: environment.aws.secret,
  },
});

@Injectable()
export class AwsS3Adapater implements FileStorageManager {
  private bucket = environment.aws.s3.bucket;
  private s3Client = new Aws.S3({});

  async generatePresignedUrlForPut(storagePath: string): Promise<string> {
    return this.s3Client.getSignedUrlPromise('putObject', {
      Bucket: this.bucket,
      Key: storagePath,
      Expires: 3 * 60 * 1000,
      ACL: 'private',
    });
  }
}
