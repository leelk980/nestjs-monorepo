import { JsonWebToken, JwtManager } from '@lib/application';
import { environment } from '@lib/common';
import { BadRequestException, Injectable, NotImplementedException } from '@nestjs/common';
import { sign, verify } from 'jsonwebtoken';

@Injectable()
export class JwtAdapter implements JwtManager {
  encode(params: JsonWebToken): string {
    switch (params.type) {
      case 'access':
        return sign(params, environment.jwt.access.secret, {
          expiresIn: environment.jwt.access.expire,
        });

      case 'refresh':
        return sign(params, environment.jwt.refresh.secret, {
          expiresIn: environment.jwt.refresh.expire,
        });

      default:
        throw new NotImplementedException(`not implemented token type. (encode)`);
    }
  }

  verify<T extends JsonWebToken['type'], U extends Extract<JsonWebToken, { type: T }>>(params: {
    type: T;
    token: string;
  }): U {
    const payload = (() => {
      switch (params.type) {
        case 'access':
          return verify(params.token, environment.jwt.access.secret);

        case 'refresh':
          return verify(params.token, environment.jwt.refresh.secret);

        default:
          throw new NotImplementedException(`not implemented token type. (decode)`);
      }
    })() as U;

    if (params.type !== payload.type) {
      throw new BadRequestException(`invalid token type. ${params.type} !== ${payload.type}`);
    }

    return payload;
  }
}
