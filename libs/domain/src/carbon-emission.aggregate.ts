import { BaseAggregate } from './base.aggregate';
import {
  CarbonEmissionFaciltityEnum,
  CarbonEmissionHeatingEnum,
  CarbonEmissionVehicleEnum,
  ObjectUtil,
  OmitProperties,
} from '@lib/common';

type CarbonEmissionProperties = OmitProperties<CarbonEmissionAggregate, 'domainEvents', 'methods'>;

export type CarbonEmissionDomainEvent = 'carbon_emission_created' | 'carbon_emission_updated';

export class CarbonEmissionAggregate extends BaseAggregate<CarbonEmissionDomainEvent> {
  startDate!: Date;
  endDate!: Date;
  electricityUsage!: number;
  electricityFee!: number;
  caluatedValue!: number;
  renewableEnergyPercent!: number;
  userId!: number;
  carbonEmissionHeatings!: {
    type: CarbonEmissionHeatingEnum;
    usage: number;
  }[];
  carbonEmissionFacilities!: {
    type: CarbonEmissionFaciltityEnum;
    count: number;
    usage: number;
  }[];
  carbonEmissionVehicles!: {
    type: CarbonEmissionVehicleEnum;
    count: number;
    usage: number;
  }[];

  static create(params: CarbonEmissionProperties) {
    const agg = new CarbonEmissionAggregate(params);
    if (agg.id === 0) agg.pushDomainEvent('carbon_emission_created');

    return agg;
  }

  update(params: Partial<CarbonEmissionProperties>) {
    Object.assign(this, ObjectUtil.removeUndefined(params));
    this.pushDomainEvent('carbon_emission_updated');

    return this;
  }
}
