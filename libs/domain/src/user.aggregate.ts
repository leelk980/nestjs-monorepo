import { BaseAggregate } from './base.aggregate';
import {
  ArrayUtil,
  DateUtil,
  ObjectUtil,
  OmitProperties,
  UnwrapArray,
  UserFileEnum,
  UserRoleEnum,
  UserTermsEnum,
  UserVerificationEnum,
} from '@lib/common';

type UserProperties = OmitProperties<UserAggregate, 'domainEvents', 'methods'>;

export type UserDomainEvent =
  | 'user_created'
  | 'user_updated'
  | 'user_verification_upserted'
  | 'user_verification_approved';

export class UserAggregate extends BaseAggregate<UserDomainEvent> {
  email!: string;
  password!: string;
  name!: string;
  jobTitle!: string;
  phone!: string;
  companyId!: number;
  userFiles!: {
    type: UserFileEnum;
    storagePath: string;
  }[];
  userVerfications!: {
    type: UserVerificationEnum;
    value: string;
    verificationCode: string;
    isVerified: boolean;
    sentAt: Date | null;
  }[];
  userRoles!: UserRoleEnum[];
  userTermses!: UserTermsEnum[];

  static create(params: UserProperties) {
    const agg = new UserAggregate(params);
    if (agg.id === 0) agg.pushDomainEvent('user_created');

    return agg;
  }

  update(params: Partial<OmitProperties<UserProperties, 'email' | 'phone' | 'userVerfications'>>) {
    Object.assign(this, ObjectUtil.removeUndefined(params));
    this.pushDomainEvent('user_updated');

    return this;
  }

  upsertUserVerfication(params: UnwrapArray<UserProperties['userVerfications']>) {
    const existingKeyIndexMap = this.userVerfications.reduce((acc, each, index) => {
      const key = `${each.type}:${each.value}`;
      acc[key] = index;

      return acc;
    }, {} as Record<string, number>);

    const index = existingKeyIndexMap[`${params.type}:${params.value}`] ?? -1;
    const existing = this.userVerfications[index];
    if (existing) {
      if (existing.isVerified) {
        return Error(`${params.type} is already verified.`);
      }
      if (
        existing.sentAt &&
        DateUtil.isBefore(new Date(), DateUtil.addMinute(existing.sentAt, 1))
      ) {
        return Error('can resend the verification code after 1 minute.');
      }

      this.userVerfications[index] = params;
    } else {
      this.userVerfications.push(params);
    }

    this.userVerfications = ArrayUtil.sortBy((each) => !each.sentAt, this.userVerfications);

    this.pushDomainEvent('user_verification_upserted');

    return this;
  }

  approveUserVerification(
    params: Pick<
      UnwrapArray<UserProperties['userVerfications']>,
      'type' | 'value' | 'verificationCode'
    >,
  ) {
    const { type, value, verificationCode } = params;

    const target = this.userVerfications.find((each) => each.type === type && each.value === value);

    if (!target) {
      return Error('invalid verification type or value.');
    }
    if (!target.sentAt || DateUtil.isAfter(new Date(), DateUtil.addMinute(target.sentAt, 30))) {
      return Error('verification time is expired.');
    }
    if (target.verificationCode !== verificationCode) {
      return Error('invalid verification code.');
    }

    if (type === 'email') {
      this.email = value;
    } else if (type === 'phone') {
      this.phone = value;
    }

    target.isVerified = true;

    // this.userVerfications.filter(
    //   (each) =>
    //     each.type !== type || (each.type === type && each.value === value && each.isVerified),
    // );

    this.pushDomainEvent('user_verification_approved');

    return this;
  }

  getUserVerifiedTypes(): UserVerificationEnum[] {
    const isEmailVerified = this.userVerfications.find(
      (each) => each.isVerified && each.type === 'email' && each.value === this.email,
    );

    const isPhoneVerified = this.userVerfications.find(
      (each) => each.isVerified && each.type === 'phone' && each.value === this.phone,
    );

    return [isEmailVerified, isPhoneVerified].reduce((acc, each, index) => {
      if (index === 0 && each) {
        acc.push('email');
      }
      if (index === 1 && each) {
        acc.push('phone');
      }

      return acc;
    }, [] as UserVerificationEnum[]);
  }
}
