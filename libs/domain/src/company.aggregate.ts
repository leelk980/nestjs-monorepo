import { BaseAggregate } from './base.aggregate';
import {
  CompanyBusinessTypeEnum,
  CompanyEmployeeSizeEnum,
  ObjectUtil,
  OmitProperties,
} from '@lib/common';

type CompanyProperties = OmitProperties<CompanyAggregate, 'domainEvents', 'methods'>;

export type CompanyDomainEvent = 'company_created' | 'company_updated';

export class CompanyAggregate extends BaseAggregate<CompanyDomainEvent> {
  name!: string;
  address!: string;
  phone!: string;
  annualSales!: number;
  companyBusinessType!: CompanyBusinessTypeEnum;
  companyEmployeeSize!: CompanyEmployeeSizeEnum;

  static create(params: CompanyProperties) {
    const agg = new CompanyAggregate(params);
    if (agg.id === 0) agg.pushDomainEvent('company_created');

    return agg;
  }

  update(params: Partial<CompanyProperties>) {
    Object.assign(this, ObjectUtil.removeUndefined(params));
    this.pushDomainEvent('company_updated');

    return this;
  }
}
