export abstract class BaseAggregate<T extends string> {
  readonly id!: number;
  readonly domainEvents: T[] = [];

  protected constructor(params: unknown) {
    Object.assign(this, params);
  }

  protected pushDomainEvent(eventName: T) {
    this.domainEvents.push(eventName);
  }
}
