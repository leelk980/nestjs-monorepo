import { HashManager } from '@lib/application';

/* eslint-disable @typescript-eslint/no-unused-vars */
export class StubHashManager implements HashManager {
  generateHash(_text: string, _rounds: number): Promise<string> {
    throw new Error('Method not implemented.');
  }

  validateHash(_text: string, _hash: string): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
}
