import { AsyncStorageItem, AsyncStorageManager } from '@lib/application';

/* eslint-disable */
export class StubAsyncStorageManager implements AsyncStorageManager {
  // @ts-ignore
  getItem<T extends AsyncStorageItem['key']>(
    _key: T,
  ): Extract<AsyncStorageItem, { key: T }>['value'] {
    throw new Error('Method not implemented.');
  }

  // @ts-ignore
  setItem<T extends AsyncStorageItem['key']>(
    _key: T,
    _value: Extract<AsyncStorageItem, { key: T }>['value'],
  ): void {
    throw new Error('Method not implemented.');
  }
}
