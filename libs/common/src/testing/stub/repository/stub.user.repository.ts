import { UserRepository } from '@lib/application';
import { UserAggregate } from '@lib/domain';

/* eslint-disable @typescript-eslint/no-unused-vars */
export class StubUserRepository implements UserRepository {
  saveOne(_agg: UserAggregate): Promise<UserAggregate> {
    throw new Error('Method not implemented.');
  }

  softDeleteOne(_agg: UserAggregate): Promise<UserAggregate> {
    throw new Error('Method not implemented.');
  }

  findOneById(_id: number): Promise<UserAggregate> {
    throw new Error('Method not implemented.');
  }

  findOneByEmailOrNull(_email: string): Promise<UserAggregate | null> {
    throw new Error('Method not implemented.');
  }

  findOneByPhoneOrNull(_email: string): Promise<UserAggregate | null> {
    throw new Error('Method not implemented.');
  }
}
