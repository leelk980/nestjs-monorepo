import { CompanyRepository } from '@lib/application';
import { CompanyAggregate } from '@lib/domain';

/* eslint-disable @typescript-eslint/no-unused-vars */
export class StubCompanyRepository implements CompanyRepository {
  saveOne(_agg: CompanyAggregate): Promise<CompanyAggregate> {
    throw new Error('Method not implemented.');
  }

  softDeleteOne(_agg: CompanyAggregate): Promise<CompanyAggregate> {
    throw new Error('Method not implemented.');
  }

  findOneById(_id: number): Promise<CompanyAggregate> {
    throw new Error('Method not implemented.');
  }
}
