import { CarbonEmissionRepository } from '@lib/application';
import { CarbonEmissionAggregate } from '@lib/domain';
import { Pagination } from '../../../global/types';

/* eslint-disable @typescript-eslint/no-unused-vars */
export class StubCarbonEmissionRepository implements CarbonEmissionRepository {
  saveOne(_agg: CarbonEmissionAggregate): Promise<CarbonEmissionAggregate> {
    throw new Error('Method not implemented.');
  }

  softDeleteOne(_agg: CarbonEmissionAggregate): Promise<CarbonEmissionAggregate> {
    throw new Error('Method not implemented.');
  }

  findMany(_params: {
    filter: { userId: number };
    page: { size: number; index: number };
  }): Promise<Pagination<CarbonEmissionAggregate>> {
    throw new Error('Method not implemented.');
  }

  findOneById(_id: number): Promise<CarbonEmissionAggregate> {
    throw new Error('Method not implemented.');
  }
}
