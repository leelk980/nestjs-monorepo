import { instance, mock, reset } from 'ts-mockito';
import { ClassType, UnwrapArray } from '../global/types';

type Stub<T extends ClassType> = ClassType<UnwrapArray<ConstructorParameters<T>>>;

export class UnitTestBuilder<T extends ClassType> {
  private injector!: T;

  private stubs!: Stub<T>[];

  static setInjector<U extends ClassType>(injector: U) {
    const builder = new UnitTestBuilder<U>();

    builder.injector = injector;

    return builder as Pick<UnitTestBuilder<U>, 'setStubs'>;
  }

  setStubs(...stubs: Stub<T>[]) {
    this.stubs = stubs;
    return this as Pick<UnitTestBuilder<T>, 'build'>;
  }

  build() {
    const stubInstances = this.stubs.map((m) => mock(m)) as ConstructorParameters<T>;
    const injectorInstance = new this.injector(
      ...stubInstances.map((mi) => instance(mi)),
    ) as InstanceType<T>;

    beforeAll(() => {
      stubInstances.forEach((mi) => reset(mi));
    });

    afterEach(() => {
      stubInstances.forEach((mi) => reset(mi));
    });

    return {
      injector: injectorInstance,
      stubs: stubInstances,
    };
  }
}
