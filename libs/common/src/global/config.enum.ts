export type UserFileEnum = 'profile_image' | 'background_image';

export type UserVerificationEnum = 'email' | 'phone';

export type UserRoleEnum = 'master' | 'admin' | 'member';

export type UserTermsEnum =
  | 'privacy_policy'
  | 'terms_of_use'
  | 'unique_identification_information'
  | 'sensitive_information';

export type CompanyBusinessTypeEnum = 'primary' | 'secondary' | 'tertiary' | 'etc';

export type CompanyEmployeeSizeEnum =
  | 'below_50'
  | 'between_50_100'
  | 'between_100_200'
  | 'between_200_300'
  | 'between_300_500'
  | 'between_500_1000'
  | 'over_1000';

export type CarbonEmissionHeatingEnum = 'lng' | 'lpg' | 'electricity';

export type CarbonEmissionFaciltityEnum = 'gasoline' | 'diesel' | 'lpg';

export type CarbonEmissionVehicleEnum = 'gasoline' | 'diesel' | 'lpg';
