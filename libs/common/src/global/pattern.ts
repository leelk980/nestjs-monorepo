export const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

export const phonePattern = /^\d{3}-\d{4}-\d{4}$/;
