export type Nullable<T> = T | null | undefined;

export type NotNullable<T> = Exclude<T, null | undefined>;

export type UnwrapArray<T> = T extends Array<infer R> ? R : never;

export type UnwrapPromise<T> = T extends Promise<infer R> ? R : never;

export type ClassType<T = unknown> = new (...params: any[]) => T;

export type AbstractClassType<T = unknown> = abstract new (...params: any[]) => T;

// export type UnionToIntersection<U> = (U extends any ? (k: U) => void : never) extends (
//   k: infer I,
// ) => void
//   ? I
//   : never;

// export type FlatProperties<T extends Record<string, any>> = UnionToIntersection<
//   {
//     [K in keyof T]: T[K] extends Record<string, any> ? T[K] : Record<K, T[K]>;
//   }[keyof T]
// >;

export type OmitProperties<
  T,
  K extends keyof T,
  U extends 'methods' | null = null,
> = U extends 'methods' ? OmitMethods<Omit<T, K>> : Omit<T, K>;

export type OmitMethods<T extends object> = Omit<
  T,
  { [key in keyof T]: T[key] extends (...params: any[]) => any ? key : never }[keyof T]
>;

export type Optional<T extends Record<string, any>> = {
  [K in keyof T]?: NotNullable<T[K]> extends Array<any>
    ? T[K] | undefined
    : NotNullable<T[K]> extends Record<string, any>
    ? Optional<T[K]>
    : T[K] | undefined;
};

export type Pagination<T> = {
  data: T[];
  page: {
    size: number;
    index: number;
    lastIndex: number;
    totalElements: number;
  };
};
