import * as F from '@fxts/core';

export class ArrayUtil {
  static assoicateBy = F.indexBy;
  static groupBy = F.groupBy;
  static sortBy = F.sortBy;

  static toUnique<T>(arr: T[]): T[] {
    return F.pipe(arr, F.uniq, F.toArray);
  }

  static uniqueBy<T>(arr: T[], fn: (each: T) => string): T[] {
    return F.pipe(arr, F.uniqBy(fn), F.toArray);
  }
}
