import { entries, filter, fromEntries, isObject, map, pipe } from '@fxts/core';
import * as typia from 'typia';

export class ObjectUtil {
  /**
   * 객체에서 `undefined` 값 속성을 제거합니다.
   * @param target `undefined` 속성을 제거할 객체입니다.
   * @returns `undefined` 속성이 제거된 객체입니다.
   */
  static removeUndefined<T extends Record<string, any>>(target: T): T {
    return pipe(
      entries(target as Record<string, any>),
      map((entry): typeof entry => {
        const [key, value] = entry;

        if (Array.isArray(value)) {
          return [
            key,
            value.map((each) => {
              if (Array.isArray(each)) {
                return each.map(this.removeUndefined);
              }

              if (isObject(each) && !(each instanceof Date)) {
                return this.removeUndefined(each);
              }

              return each;
            }),
          ];
        }

        if (isObject(value) && !(value instanceof Date)) {
          return [key, this.removeUndefined(value)];
        }

        return value !== undefined ? [key, value] : ['_removed_', ''];
      }),
      filter((each) => each[0] !== '_removed_'),
      fromEntries,
    ) as T;
  }

  static clone = typia.clone;
  static prune = typia.assertPrune;
}
