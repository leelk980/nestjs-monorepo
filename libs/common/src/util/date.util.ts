import { BadRequestException } from '@nestjs/common';
import * as dayjs from 'dayjs';

type Datable = Date | string;

export class DateUtil {
  static addMinute(target: Datable, minute: number) {
    return dayjs(target).add(minute, 'minute').toDate();
  }

  static minusMinute(target: Datable, minute: number) {
    return dayjs(target).subtract(minute, 'minute').toDate();
  }

  static isBefore(start: Datable, end: Datable) {
    return dayjs(start).isBefore(end);
  }

  static isAfter(end: Datable, start: Datable) {
    return dayjs(start).isBefore(end);
  }

  static toDateString(date: Date) {
    const dateString = date.toISOString().split('T')[0];
    if (!dateString) {
      throw new BadRequestException(`cannot convert to date string. ${date}`);
    }

    return dateString;
  }
}
