import { ObjectUtil } from './object.util';

describe('ObjectUtil', () => {
  describe('removeUndefined', () => {
    test('should remove undefined properties from a nested object', () => {
      const obj = {
        name: 'John',
        age: undefined,
        email: 'john@example.com',
        address: {
          street: '123 Main St',
          city: 'New York',
          state: undefined,
        },
      };

      const result = ObjectUtil.removeUndefined(obj);

      expect(result).toEqual({
        name: 'John',
        email: 'john@example.com',
        address: {
          street: '123 Main St',
          city: 'New York',
        },
      });
    });

    test('should remove undefined properties from an object without array item', () => {
      const obj = {
        name: 'John',
        age: undefined,
        email: 'john@example.com',
        hobbies: ['reading', undefined, 'cooking'],
      };

      const result = ObjectUtil.removeUndefined(obj);

      expect(result).toEqual({
        name: 'John',
        email: 'john@example.com',
        hobbies: ['reading', undefined, 'cooking'],
      });
    });

    test('should handle null and Date objects without removing them', () => {
      const obj = {
        name: 'John',
        age: null,
        email: 'john@example.com',
        createdAt: new Date(),
      };

      const result = ObjectUtil.removeUndefined(obj);

      expect(result).toEqual(obj);
    });

    test('should handle array within object and remove undefined properties', () => {
      const obj = {
        name: 'John',
        age: undefined,
        email: 'john@example.com',
        address: {
          street: '123 Main St',
          city: 'New York',
          state: undefined,
          contacts: [
            { name: 'Alice', phone: '123-456' },
            { name: 'Bob', phone: undefined },
            { name: 'Charlie', phone: '789-012' },
          ],
          examples: [
            [
              { name: 'Alice', phone: '123-456' },
              { name: 'Bob', phone: undefined },
              { name: 'Charlie', phone: '789-012' },
            ],
            [
              { name: 'Alice', phone: '123-456' },
              { name: 'Bob', phone: undefined },
              { name: 'Charlie', phone: '789-012' },
            ],
          ],
        },
      };

      const result = ObjectUtil.removeUndefined(obj);

      expect(result).toEqual({
        name: 'John',
        email: 'john@example.com',
        address: {
          street: '123 Main St',
          city: 'New York',
          contacts: [
            { name: 'Alice', phone: '123-456' },
            { name: 'Bob' },
            { name: 'Charlie', phone: '789-012' },
          ],
          examples: [
            [
              { name: 'Alice', phone: '123-456' },
              { name: 'Bob' },
              { name: 'Charlie', phone: '789-012' },
            ],
            [
              { name: 'Alice', phone: '123-456' },
              { name: 'Bob' },
              { name: 'Charlie', phone: '789-012' },
            ],
          ],
        },
      });
    });
  });
});
