import { Pagination } from '@lib/common';
import { CarbonEmissionAggregate } from '@lib/domain';

export abstract class CarbonEmissionRepository {
  abstract saveOne(agg: CarbonEmissionAggregate): Promise<CarbonEmissionAggregate>;

  abstract softDeleteOne(agg: CarbonEmissionAggregate): Promise<CarbonEmissionAggregate>;

  abstract findMany(params: {
    filter: { userId: number };
    page: { size: number; index: number };
  }): Promise<Pagination<CarbonEmissionAggregate>>;

  abstract findOneById(id: number): Promise<CarbonEmissionAggregate>;
}
