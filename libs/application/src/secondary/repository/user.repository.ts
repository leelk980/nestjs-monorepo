import { UserAggregate } from '@lib/domain';

export abstract class UserRepository {
  abstract saveOne(agg: UserAggregate): Promise<UserAggregate>;

  abstract softDeleteOne(agg: UserAggregate): Promise<UserAggregate>;

  abstract findOneById(id: number): Promise<UserAggregate>;

  abstract findOneByEmailOrNull(email: string): Promise<UserAggregate | null>;

  abstract findOneByPhoneOrNull(phone: string): Promise<UserAggregate | null>;
}
