import { CompanyAggregate } from '@lib/domain';

export abstract class CompanyRepository {
  abstract saveOne(agg: CompanyAggregate): Promise<CompanyAggregate>;

  abstract softDeleteOne(agg: CompanyAggregate): Promise<CompanyAggregate>;

  abstract findOneById(id: number): Promise<CompanyAggregate>;
}
