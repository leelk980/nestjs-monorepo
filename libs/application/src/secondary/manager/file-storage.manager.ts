export abstract class FileStorageManager {
  abstract generatePresignedUrlForPut(storagePath: string): Promise<string>;
}
