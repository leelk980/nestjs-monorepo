export abstract class HashManager {
  abstract generateHash(text: string, rounds: number): Promise<string>;

  abstract validateHash(text: string, hash: string): Promise<boolean>;
}
