import { UserRoleEnum, UserVerificationEnum } from '@lib/common';

export type JsonWebToken =
  | (
      | {
          type: 'access';
          userId: number;
          userVerficiations: UserVerificationEnum[];
          userRoles: UserRoleEnum[];
        }
      | {
          type: 'refresh';
          userId: number;
          test: string;
        }
    ) & {
      iat?: number;
      exp?: number;
    };

export abstract class JwtManager {
  abstract encode(params: JsonWebToken): string;

  abstract verify<
    T extends JsonWebToken['type'],
    U extends Extract<JsonWebToken, { type: T }>,
  >(params: { type: T; token: string }): U;
}
