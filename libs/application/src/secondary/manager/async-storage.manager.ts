import { UserRoleEnum, UserVerificationEnum } from '@lib/common';

export type AsyncStorageItem =
  | {
      key: 'transaction';
      value: unknown;
    }
  | {
      key: 'currUser';
      value: {
        id: number;
        userVerficiations: UserVerificationEnum[];
        userRoles: UserRoleEnum[];
      };
    };

export abstract class AsyncStorageManager {
  abstract getItem<T extends AsyncStorageItem['key']>(
    key: T,
  ): Extract<AsyncStorageItem, { key: T }>['value'];

  abstract setItem<T extends AsyncStorageItem['key']>(
    key: T,
    value: Extract<AsyncStorageItem, { key: T }>['value'],
  ): void;
}
