export interface PrimaryPort {
  execute(data: unknown): Promise<unknown>;
}
