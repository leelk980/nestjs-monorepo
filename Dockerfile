FROM node:18-alpine

WORKDIR /renew-us-backend

COPY package*.json ./

RUN npm install

COPY . .

ARG NODE_ENV

ENV NODE_ENV=${NODE_ENV}

EXPOSE 3000 

RUN npm run prepare

RUN npm run gen:prisma

RUN npm run build

